package com.example.warehousemobileclient.service;

import com.example.warehousemobileclient.model.TSDetail;
import com.example.warehousemobileclient.model.TSHeader;
import com.example.warehousemobileclient.model.TallyData;
import com.example.warehousemobileclient.model.User;
import com.example.warehousemobileclient.model.WIDataDetail;
import com.example.warehousemobileclient.model.WIDataGeneral;
import com.example.warehousemobileclient.model.WIDataHeader;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IWarehouseApi {
    @GET("wrdataheader")
    Call<List<WRDataHeader>> getWRDataHeaders();
    @GET("wrdataheader/GetByID/{id}")
    Call<List<WRDataHeader>> getWRDataHeader(@Path("id") String id);
    @PUT("wrdataheader/Put/{wRDNumber}")
    Call<WRDataHeader> updateWRDataHeader(@Path("wRDNumber") String wRDNumber,@Body WRDataHeader body);

    @GET("wrdatadetail/GetByID/{id}")
    Call<List<WRDataDetail>> getWRDataDetailById(@Path("id") String id);
    @GET("wrdatadetail/GetByIDCode/{wRDNumber}/{goodsID}/{idCode}")
    Call<List<WRDataDetail>> getWRDataDetailByIdCode(@Path("wRDNumber") String wRDNumber,@Path("goodsID") String goodsID,@Path("idCode") String idCode);
    @PUT("wrdatadetail/Put/{wRDNumber}/{goodsID}/{ordinal}")
    Call<WRDataDetail> updateWRDataDetail(@Path("wRDNumber") String wRDNumber, @Path("goodsID") String goodsID, @Path("ordinal") int ordinal, @Body WRDataDetail body);

    @GET("wrdatageneral/getByID/{id}")
    Call<List<WRDataGeneral>> getWRDataGeneralById(@Path("id") String id);
    @PUT("wrdatageneral/Put/{wRDNumber}/{Barcode}/{ordinal}")
    Call<WRDataGeneral> updateWRDataGeneral(@Path("wRDNumber") String wRDNumber, @Path("Barcode") String Barcode, @Path("ordinal") int ordinal, @Body WRDataGeneral body);
    @GET("wrdatageneral/GetByWRDNumberAndBarcode/{wRDNumber}/{Barcode}")
    Call<List<WRDataGeneral>> getWRDataGeneralByBarcode(@Path("wRDNumber") String wRDNumber,@Path("Barcode") String Barcode);

    @GET("widataheader")
    Call<List<WIDataHeader>> getWIDataHeaders();
    @GET("widataheader/GetByID/{id}")
    Call<List<WIDataHeader>> getWIDataHeader(@Path("id") String id);
    @PUT("widataheader/Put/{wIDNumber}")
    Call<WIDataHeader> updateWIDataHeader(@Path("wIDNumber") String wIDNumber,@Body WIDataHeader body);

    @GET("widatadetail/GetByID/{id}")
    Call<List<WIDataDetail>> getWIDataDetailById(@Path("id") String id);
    @GET("widatadetail/GetByIDCode/{wIDNumber}/{goodsID}/{idCode}")
    Call<List<WIDataDetail>> getWIDataDetailByIdCode(@Path("wIDNumber") String wIDNumber,@Path("goodsID") String goodsID,@Path("idCode") String idCode);
    @PUT("widatadetail/Put/{wIDNumber}/{goodsID}/{ordinal}")
    Call<WIDataDetail> updateWIDataDetail(@Path("wIDNumber") String wIDNumber, @Path("goodsID") String goodsID, @Path("ordinal") int ordinal, @Body WIDataDetail body);

    @GET("widatageneral/getByID/{id}")
    Call<List<WIDataGeneral>> getWIDataGeneralById(@Path("id") String id);
    @PUT("widatageneral/Put/{wIDNumber}/{goodsID}/{ordinal}")
    Call<WIDataGeneral> updateWIDataGeneral(@Path("wIDNumber") String wIDNumber, @Path("goodsID") String goodsID, @Path("ordinal") int ordinal, @Body WIDataGeneral body);
    @GET("widatageneral/GetByWIDNumberAndGoodsID/{wIDNumber}/{goodsID}")
    Call<List<WIDataGeneral>> getWIDataGeneralByGoods(@Path("wIDNumber") String wIDNumber,@Path("goodsID") String goodsID);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("user/login")
    Call<User> login(@Body User body);

    @GET("tallydata/GetByID/{id}")
    Call<List<TallyData>> getTallyDataById(@Path("id") String id);
    @GET("tallydata/lastID")
    Call<String> getLastTSNumber();
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tallydata/Post")
    Call<TallyData> creatTallyData(@Body TallyData body);
    @PUT("tallydata/Put/{tsNumber}/{GoodsID}/{No}")
    Call<TallyData> updateTallyData(@Path("tsNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("No") int no,@Body TallyData body);
    @DELETE("tallydata/Delete/{tsNumber}")
    Call<Boolean> deleteTallyData(@Path("tsNumber") String tsNumber);
    @DELETE("tallydata/DeleteDetail/{tsNumber}/{goodsID}/{No}")
    Call<Boolean> deleteTallyDataDetail(@Path("tsNumber") String tsNumber,@Path("goodsID") String goodsID,@Path("No") int No);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tsheader/Post")
    Call<TSHeader> creatTSHeader(@Body TSHeader body);
    @PUT("tsheader/Put/{tsNumber}")
    Call<TSHeader> updateTSHeader(@Path("tsNumber") String tsNumber,@Body TSHeader body);
    @GET("tsheader/GetByID/{id}")
    Call<List<TSHeader>> getTSHeaderById(@Path("id") String id);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tsdetail/Post")
    Call<TSDetail> creatTSDetail(@Body TSDetail body);
    @PUT("tsdetail/Put/{tsNumber}/{GoodsID}/{Ordinal}")
    Call<TSDetail> updateTSDetail(@Path("tsNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("Ordinal") int ordinal,@Body TSDetail body);
    @GET("tsdetail/GetByMultiID/{TSNumber}/{GoodsID}/{Ordinal}")
    Call<List<TSDetail>> getTSDetailByMultiId(@Path("TSNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("Ordinal") int ordinal);
    @GET("tsdetail/GetByID/{id}")
    Call<List<TSDetail>> getTSDetailById(@Path("id") String id);
}

