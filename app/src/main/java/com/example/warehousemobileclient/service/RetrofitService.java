package com.example.warehousemobileclient.service;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.warehousemobileclient.helper.Common;
import com.example.warehousemobileclient.ui.setting.SettingFragment;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.warehousemobileclient.ui.setting.SettingFragment.PREFERENCES;

public class RetrofitService {
    public static String BASE_URL = "http://192.168.1.182:6375/api/";
//private static final String BASE_URL = "http://www.wmservice.hp.com:6379/api/";
    public static IWarehouseApi getService(){
        return APIRetrofitClient.getClient(Common.IP_SERVER).create(IWarehouseApi.class);
    }

//    public static DataService Create(){
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://192.168.1.55:6379/api/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        return retrofit.create(DataService.class);
//    }
}
