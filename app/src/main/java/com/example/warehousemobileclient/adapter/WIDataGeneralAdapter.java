package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.WIDataGeneral;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class WIDataGeneralAdapter extends RecyclerView.Adapter<WIDataGeneralAdapter.ViewHolder> {
    Context context;
    ArrayList<WIDataGeneral> list;
    WIDataGeneral entry;
    WIDataGeneralAdapter.ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WIDataGeneralAdapter(Context context, ArrayList<WIDataGeneral> list, WIDataGeneralAdapter.ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public WIDataGeneralAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdata_general_barcode_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WIDataGeneralAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WIDataGeneralAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        entry = list.get(i);
        viewHolder.tvGoodsID.setText(entry.getGoodsID());
        viewHolder.tvQuantity.setText(entry.getQuantity() + "");
        viewHolder.tvQuantityOrg.setText(entry.getQuantityOrg() + "");
        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+"");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });


        if(entry.getQuantity().doubleValue()==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if(entry.getQuantity().doubleValue()>0&&entry.getQuantity().doubleValue()<entry.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f75c78"));
        }
        else if(entry.getQuantity().doubleValue()==entry.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#1dde3d"));
        } else if(entry.getQuantity().doubleValue()>entry.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f7fc60"));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvGoodsID, tvQuantity,tvQuantityOrg,tvLineNo;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvGoodsID = itemView.findViewById(R.id.tvGoodsID);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvQuantityOrg = itemView.findViewById(R.id.tvQuantityOrg);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<WIDataGeneral> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WIDataGeneral wrDataGeneral);
    }

}
