package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.WRDataGeneral;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class WRDataGeneralRFIDAdapter extends RecyclerView.Adapter<WRDataGeneralRFIDAdapter.ViewHolder> {
    private static final int UNSELECTED = -1;
    private int selectedItem = UNSELECTED;
    Context context;
    ArrayList<WRDataGeneral> list;
    WRDataGeneral wrDataGeneral;
    WRDataGeneralRFIDAdapter.ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WRDataGeneralRFIDAdapter(Context context, ArrayList<WRDataGeneral> list, WRDataGeneralRFIDAdapter.ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public WRDataGeneralRFIDAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdata_general_rfid_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WRDataGeneralRFIDAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WRDataGeneralRFIDAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        wrDataGeneral = list.get(i);
        viewHolder.tvBarcode.setText(wrDataGeneral.getBarcode());
        viewHolder.tvSku.setText(wrDataGeneral.getSku());
        viewHolder.tvSize.setText(wrDataGeneral.getSize());
        viewHolder.tvQuantity.setText(String.format("%.0f",wrDataGeneral.getQuantity()));
        viewHolder.tvQuantityOrg.setText(String.format("%.0f",wrDataGeneral.getQuantityOrg()));
        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+".");
        }
        viewHolder.expandableLayout.setInterpolator(new OvershootInterpolator());
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });

        int position = i;
        boolean isSelected = position == selectedItem;

        viewHolder.expandButton.setSelected(isSelected);
        viewHolder.expandableLayout.setExpanded(isSelected, false);

        viewHolder.expandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewHolder != null) {
                    viewHolder.expandButton.setSelected(false);
                    viewHolder.expandableLayout.collapse();
                    viewHolder.expandButton.setBackgroundResource(R.drawable.ic_baseline_chevron_right_24);
                }
                int position = i;
                if (position == selectedItem) {
                    selectedItem = UNSELECTED;
                }
                else {
                    viewHolder.expandButton.setSelected(true);
                    viewHolder.expandableLayout.expand();
                    viewHolder.expandButton.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_down_24);
                    selectedItem = position;
                }
            }
        });



        if(wrDataGeneral.getQuantity()==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if(wrDataGeneral.getQuantity()>0&&wrDataGeneral.getQuantity()<wrDataGeneral.getQuantityOrg()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f75c78"));
        }
        else if(wrDataGeneral.getQuantity()==wrDataGeneral.getQuantityOrg()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#1dde3d"));
        } else if(wrDataGeneral.getQuantity()>wrDataGeneral.getQuantityOrg()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f7fc60"));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSku,tvSize, tvQuantity,tvQuantityOrg,tvLineNo,tvBarcode;
        public CardView cardView;
        public ExpandableLayout expandableLayout;
        public TextView expandButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvBarcode= itemView.findViewById(R.id.tvBarcode);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvQuantityOrg = itemView.findViewById(R.id.tvQuantityOrg);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
            tvSku=itemView.findViewById(R.id.tvSku);
            tvSize=itemView.findViewById(R.id.tvSize);
            expandableLayout= itemView.findViewById(R.id.expandable_layout);
            expandButton=itemView.findViewById(R.id.expandButton);
        }
    }

    public void updateList(List<WRDataGeneral> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WRDataGeneral wrDataGeneral);
    }

}
