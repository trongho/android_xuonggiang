package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.WRDataGeneral;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class WRDataGeneralBarcodeAdapter extends RecyclerView.Adapter<WRDataGeneralBarcodeAdapter.ViewHolder> {
    Context context;
    ArrayList<WRDataGeneral> list;
    WRDataGeneral wrDataGeneral;
    WRDataGeneralBarcodeAdapter.ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WRDataGeneralBarcodeAdapter(Context context, ArrayList<WRDataGeneral> list, WRDataGeneralBarcodeAdapter.ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public WRDataGeneralBarcodeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdata_general_barcode_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WRDataGeneralBarcodeAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WRDataGeneralBarcodeAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        wrDataGeneral = list.get(i);
        viewHolder.tvBarcode.setText(wrDataGeneral.getBarcode());
        viewHolder.tvSku.setText(wrDataGeneral.getSku());
        viewHolder.tvSize.setText(wrDataGeneral.getSize());
        viewHolder.tvQuantity.setText(String.format("%.0f",wrDataGeneral.getQuantity()));
        viewHolder.tvQuantityOrg.setText(String.format("%.0f",wrDataGeneral.getQuantityOrg()));
        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+"");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });


        if(wrDataGeneral.getQuantity()==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if(wrDataGeneral.getQuantity()>0&&wrDataGeneral.getQuantity()<wrDataGeneral.getQuantityOrg()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f75c78"));
        }
        else if(wrDataGeneral.getQuantity()==wrDataGeneral.getQuantityOrg()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#1dde3d"));
        } else if(wrDataGeneral.getQuantity()>wrDataGeneral.getQuantityOrg()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f7fc60"));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvBarcode,tvSku,tvSize, tvQuantity,tvQuantityOrg,tvLineNo;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvBarcode = itemView.findViewById(R.id.tvBarcode);
            tvSku = itemView.findViewById(R.id.tvSku);
            tvSize = itemView.findViewById(R.id.tvSize);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvQuantityOrg = itemView.findViewById(R.id.tvQuantityOrg);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<WRDataGeneral> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WRDataGeneral wrDataGeneral);
    }

}
