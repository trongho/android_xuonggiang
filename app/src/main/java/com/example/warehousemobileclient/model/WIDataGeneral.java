package com.example.warehousemobileclient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WIDataGeneral {
    @SerializedName("widNumber")
    @Expose
    private String widNumber;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("goodsID")
    @Expose
    private String goodsID;
    @SerializedName("goodsName")
    @Expose
    private String goodsName;
    @SerializedName("idCode")
    @Expose
    private String idCode;
    @SerializedName("locationID")
    @Expose
    private String locationID;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("totalQuantity")
    @Expose
    private Double totalQuantity;
    @SerializedName("totalGoods")
    @Expose
    private Double totalGoods;
    @SerializedName("quantityOrg")
    @Expose
    private Double quantityOrg;
    @SerializedName("totalQuantityOrg")
    @Expose
    private Double totalQuantityOrg;
    @SerializedName("totalGoodsOrg")
    @Expose
    private Double totalGoodsOrg;
    @SerializedName("locationIDOrg")
    @Expose
    private String locationIDOrg;
    @SerializedName("creatorID")
    @Expose
    private Object creatorID;
    @SerializedName("createdDateTime")
    @Expose
    private Object createdDateTime;
    @SerializedName("editerID")
    @Expose
    private String editerID;
    @SerializedName("editedDateTime")
    @Expose
    private String editedDateTime;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("packingVolume")
    @Expose
    private Double packingVolume;
    @SerializedName("quantityByPack")
    @Expose
    private Double quantityByPack;
    @SerializedName("quantityByItem")
    @Expose
    private Double quantityByItem;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("scanOption")
    @Expose
    private Integer scanOption;
    @SerializedName("packingQuantity")
    @Expose
    private Double packingQuantity;

    public String getWidNumber() {
        return widNumber;
    }

    public void setWidNumber(String widNumber) {
        this.widNumber = widNumber;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Double getTotalGoods() {
        return totalGoods;
    }

    public void setTotalGoods(Double totalGoods) {
        this.totalGoods = totalGoods;
    }

    public Double getQuantityOrg() {
        return quantityOrg;
    }

    public void setQuantityOrg(Double quantityOrg) {
        this.quantityOrg = quantityOrg;
    }

    public Double getTotalQuantityOrg() {
        return totalQuantityOrg;
    }

    public void setTotalQuantityOrg(Double totalQuantityOrg) {
        this.totalQuantityOrg = totalQuantityOrg;
    }

    public Double getTotalGoodsOrg() {
        return totalGoodsOrg;
    }

    public void setTotalGoodsOrg(Double totalGoodsOrg) {
        this.totalGoodsOrg = totalGoodsOrg;
    }

    public String getLocationIDOrg() {
        return locationIDOrg;
    }

    public void setLocationIDOrg(String locationIDOrg) {
        this.locationIDOrg = locationIDOrg;
    }

    public Object getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(Object creatorID) {
        this.creatorID = creatorID;
    }

    public Object getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Object createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getEditerID() {
        return editerID;
    }

    public void setEditerID(String editerID) {
        this.editerID = editerID;
    }

    public String getEditedDateTime() {
        return editedDateTime;
    }

    public void setEditedDateTime(String editedDateTime) {
        this.editedDateTime = editedDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getPackingVolume() {
        return packingVolume;
    }

    public void setPackingVolume(Double packingVolume) {
        this.packingVolume = packingVolume;
    }

    public Double getQuantityByPack() {
        return quantityByPack;
    }

    public void setQuantityByPack(Double quantityByPack) {
        this.quantityByPack = quantityByPack;
    }

    public Double getQuantityByItem() {
        return quantityByItem;
    }

    public void setQuantityByItem(Double quantityByItem) {
        this.quantityByItem = quantityByItem;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getScanOption() {
        return scanOption;
    }

    public void setScanOption(Integer scanOption) {
        this.scanOption = scanOption;
    }

    public Double getPackingQuantity() {
        return packingQuantity;
    }

    public void setPackingQuantity(Double packingQuantity) {
        this.packingQuantity = packingQuantity;
    }

}
