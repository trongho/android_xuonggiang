package com.example.warehousemobileclient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class WRDataHeader {
    @SerializedName("wrdNumber")
    @Expose
    private String wrdNumber;
    @SerializedName("wrdDate")
    @Expose
    private String wrdDate;
    @SerializedName("wrrNumber")
    @Expose
    private String wrrNumber;
    @SerializedName("handlingStatusID")
    @Expose
    private String handlingStatusID;
    @SerializedName("handlingStatusName")
    @Expose
    private String handlingStatusName;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("branchID")
    @Expose
    private String branchID;
    @SerializedName("branchName")
    @Expose
    private String branchName;
    @SerializedName("totalQuantity")
    @Expose
    private Double totalQuantity;
    @SerializedName("totalQuantityOrg")
    @Expose
    private Double totalQuantityOrg;
    @SerializedName("totalGoods")
    @Expose
    private Double totalGoods;
    @SerializedName("totalGoodsOrg")
    @Expose
    private Double totalGoodsOrg;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdUserID")
    @Expose
    private String createdUserID;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("updatedUserID")
    @Expose
    private String updatedUserID;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    public WRDataHeader() {
    }

    public WRDataHeader(String wrdNumber, String wrdDate, String wrrNumber, String handlingStatusID, String handlingStatusName, String note, String branchID, String branchName, Double totalQuantity, Double totalQuantityOrg, Double totalGoods, Double totalGoodsOrg, String status, String createdUserID, String createdDate, String updatedUserID, String updatedDate) {
        this.wrdNumber = wrdNumber;
        this.wrdDate = wrdDate;
        this.wrrNumber = wrrNumber;
        this.handlingStatusID = handlingStatusID;
        this.handlingStatusName = handlingStatusName;
        this.note = note;
        this.branchID = branchID;
        this.branchName = branchName;
        this.totalQuantity = totalQuantity;
        this.totalQuantityOrg = totalQuantityOrg;
        this.totalGoods = totalGoods;
        this.totalGoodsOrg = totalGoodsOrg;
        this.status = status;
        this.createdUserID = createdUserID;
        this.createdDate = createdDate;
        this.updatedUserID = updatedUserID;
        this.updatedDate = updatedDate;
    }

    public String getWrdNumber() {
        return wrdNumber;
    }

    public void setWrdNumber(String wrdNumber) {
        this.wrdNumber = wrdNumber;
    }

    public String getWrdDate() {
        return wrdDate;
    }

    public void setWrdDate(String wrdDate) {
        this.wrdDate = wrdDate;
    }


    public String getWrrNumber() {
        return wrrNumber;
    }

    public void setWrrNumber(String wrrNumber) {
        this.wrrNumber = wrrNumber;
    }
    public String getHandlingStatusID() {
        return handlingStatusID;
    }

    public void setHandlingStatusID(String handlingStatusID) {
        this.handlingStatusID = handlingStatusID;
    }

    public String getHandlingStatusName() {
        return handlingStatusName;
    }

    public void setHandlingStatusName(String handlingStatusName) {
        this.handlingStatusName = handlingStatusName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBranchID() {
        return branchID;
    }

    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Double getTotalQuantityOrg() {
        return totalQuantityOrg;
    }

    public Double getTotalGoods() {
        return totalGoods;
    }

    public void setTotalGoods(Double totalGoods) {
        this.totalGoods = totalGoods;
    }

    public Double getTotalGoodsOrg() {
        return totalGoodsOrg;
    }

    public void setTotalGoodsOrg(Double totalGoodsOrg) {
        this.totalGoodsOrg = totalGoodsOrg;
    }

    public void setTotalQuantityOrg(Double totalQuantityOrg) {
        this.totalQuantityOrg = totalQuantityOrg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedUserID() {
        return createdUserID;
    }

    public void setCreatedUserID(String createdUserID) {
        this.createdUserID = createdUserID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedUserID() {
        return updatedUserID;
    }

    public void setUpdatedUserID(String updatedUserID) {
        this.updatedUserID = updatedUserID;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
