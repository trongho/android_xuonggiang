package com.example.warehousemobileclient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TSDetail {
    @SerializedName("tsNumber")
    @Expose
    private String tsNumber;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("goodsID")
    @Expose
    private String goodsID;
    @SerializedName("goodsName")
    @Expose
    private String goodsName;
    @SerializedName("tallyUnitID")
    @Expose
    private String tallyUnitID;
    @SerializedName("unitRate")
    @Expose
    private Double unitRate;
    @SerializedName("stockUnitID")
    @Expose
    private String stockUnitID;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("priceIncludedVAT")
    @Expose
    private Double priceIncludedVAT;
    @SerializedName("priceExcludedVAT")
    @Expose
    private Double priceExcludedVAT;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("discountAmount")
    @Expose
    private Double discountAmount;
    @SerializedName("vat")
    @Expose
    private Double vat;
    @SerializedName("vatAmount")
    @Expose
    private Double vatAmount;
    @SerializedName("amountExcludedVAT")
    @Expose
    private Double amountExcludedVAT;
    @SerializedName("amountIncludedVAT")
    @Expose
    private Double amountIncludedVAT;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("expiryDate")
    @Expose
    private String expiryDate;
    @SerializedName("serialNumber")
    @Expose
    private String serialNumber;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("status")
    @Expose
    private String status;

    public TSDetail() {
    }

    public TSDetail(String tsNumber, Integer ordinal, String goodsID, String goodsName, String tallyUnitID, Double unitRate, String stockUnitID, Double quantity, Double priceIncludedVAT, Double priceExcludedVAT, Double price, Double discount, Double discountAmount, Double vat, Double vatAmount, Double amountExcludedVAT, Double amountIncludedVAT, Double amount, String expiryDate, String serialNumber, String note, String status) {
        this.tsNumber = tsNumber;
        this.ordinal = ordinal;
        this.goodsID = goodsID;
        this.goodsName = goodsName;
        this.tallyUnitID = tallyUnitID;
        this.unitRate = unitRate;
        this.stockUnitID = stockUnitID;
        this.quantity = quantity;
        this.priceIncludedVAT = priceIncludedVAT;
        this.priceExcludedVAT = priceExcludedVAT;
        this.price = price;
        this.discount = discount;
        this.discountAmount = discountAmount;
        this.vat = vat;
        this.vatAmount = vatAmount;
        this.amountExcludedVAT = amountExcludedVAT;
        this.amountIncludedVAT = amountIncludedVAT;
        this.amount = amount;
        this.expiryDate = expiryDate;
        this.serialNumber = serialNumber;
        this.note = note;
        this.status = status;
    }

    public String getTsNumber() {
        return tsNumber;
    }

    public void setTsNumber(String tsNumber) {
        this.tsNumber = tsNumber;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getTallyUnitID() {
        return tallyUnitID;
    }

    public void setTallyUnitID(String tallyUnitID) {
        this.tallyUnitID = tallyUnitID;
    }

    public Double getUnitRate() {
        return unitRate;
    }

    public void setUnitRate(Double unitRate) {
        this.unitRate = unitRate;
    }

    public String getStockUnitID() {
        return stockUnitID;
    }

    public void setStockUnitID(String stockUnitID) {
        this.stockUnitID = stockUnitID;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPriceIncludedVAT() {
        return priceIncludedVAT;
    }

    public void setPriceIncludedVAT(Double priceIncludedVAT) {
        this.priceIncludedVAT = priceIncludedVAT;
    }

    public Double getPriceExcludedVAT() {
        return priceExcludedVAT;
    }

    public void setPriceExcludedVAT(Double priceExcludedVAT) {
        this.priceExcludedVAT = priceExcludedVAT;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Double getVat() {
        return vat;
    }

    public void setVat(Double vat) {
        this.vat = vat;
    }

    public Double getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(Double vatAmount) {
        this.vatAmount = vatAmount;
    }

    public Double getAmountExcludedVAT() {
        return amountExcludedVAT;
    }

    public void setAmountExcludedVAT(Double amountExcludedVAT) {
        this.amountExcludedVAT = amountExcludedVAT;
    }

    public Double getAmountIncludedVAT() {
        return amountIncludedVAT;
    }

    public void setAmountIncludedVAT(Double amountIncludedVAT) {
        this.amountIncludedVAT = amountIncludedVAT;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
