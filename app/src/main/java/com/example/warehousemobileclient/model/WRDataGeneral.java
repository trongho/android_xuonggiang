package com.example.warehousemobileclient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WRDataGeneral {
    @SerializedName("wrdNumber")
    @Expose
    private String wrdNumber;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("barcode")
    @Expose
    private String barcode;
    @SerializedName("rfidTagID")
    @Expose
    private String rfidTagID;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("inputDate")
    @Expose
    private String inputDate;
    @SerializedName("styleColorSize")
    @Expose
    private String styleColorSize;

    @SerializedName("style")
    @Expose
    private String style;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("Size")
    @Expose
    private String size;
    @SerializedName("quantityOrg")
    @Expose
    private int quantityOrg;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("productGroup")
    @Expose
    private String productGroup;
    @SerializedName("descriptionVN")
    @Expose
    private String descriptionVN;
    @SerializedName("lifeStylePerformance")
    @Expose
    private String lifeStylePerformance;
    @SerializedName("div")
    @Expose
    private String div;
    @SerializedName("outsole")
    @Expose
    private String outsole;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("productline")
    @Expose
    private String productline;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("exWorkUSD")
    @Expose
    private Double exWorkUSD;
    @SerializedName("exWorkVND")
    @Expose
    private Double exWorkVND;
    @SerializedName("amountVND")
    @Expose
    private Double amountVND;
    @SerializedName("retailPrice")
    @Expose
    private Double retailPrice;
    @SerializedName("season")
    @Expose
    private String season;
    @SerializedName("coo")
    @Expose
    private String coo;
    @SerializedName("material")
    @Expose
    private String material;
    @SerializedName("invoiceNo")
    @Expose
    private String invoiceNo;
    @SerializedName("dateOfInvoice")
    @Expose
    private String dateOfInvoice;
    @SerializedName("saleContract")
    @Expose
    private String saleContract;
    @SerializedName("shipment")
    @Expose
    private String shipment;
    @SerializedName("factoryAdress")
    @Expose
    private String factoryAdress;
    @SerializedName("factoryName")
    @Expose
    private String factoryName;
    @SerializedName("factoryNameVN")
    @Expose
    private String factoryNameVN;
    @SerializedName("factoryAdressVN")
    @Expose
    private String factoryAdressVN;
    @SerializedName("newPrice")
    @Expose
    private Double newPrice;
    @SerializedName("changePrice")
    @Expose
    private String changePrice;

    public WRDataGeneral() {
    }

    public WRDataGeneral(String wrdNumber, Integer ordinal, String barcode, String rfidTagID, String status, String inputDate, String styleColorSize, String style, String color, String sku, String size, int quantityOrg, int quantity, String gender, String productGroup, String descriptionVN, String lifeStylePerformance, String div, String outsole, String category, String productline, String description, Double exWorkUSD, Double exWorkVND, Double amountVND, Double retailPrice, String season, String coo, String material, String invoiceNo, String dateOfInvoice, String saleContract, String shipment, String factoryAdress, String factoryName, String factoryNameVN, String factoryAdressVN, Double newPrice, String changePrice) {
        this.wrdNumber = wrdNumber;
        this.ordinal = ordinal;
        this.barcode = barcode;
        this.rfidTagID = rfidTagID;
        this.status = status;
        this.inputDate = inputDate;
        this.styleColorSize = styleColorSize;
        this.style = style;
        this.color = color;
        this.sku = sku;
        this.size = size;
        this.quantityOrg = quantityOrg;
        this.quantity = quantity;
        this.gender = gender;
        this.productGroup = productGroup;
        this.descriptionVN = descriptionVN;
        this.lifeStylePerformance = lifeStylePerformance;
        this.div = div;
        this.outsole = outsole;
        this.category = category;
        this.productline = productline;
        this.description = description;
        this.exWorkUSD = exWorkUSD;
        this.exWorkVND = exWorkVND;
        this.amountVND = amountVND;
        this.retailPrice = retailPrice;
        this.season = season;
        this.coo = coo;
        this.material = material;
        this.invoiceNo = invoiceNo;
        this.dateOfInvoice = dateOfInvoice;
        this.saleContract = saleContract;
        this.shipment = shipment;
        this.factoryAdress = factoryAdress;
        this.factoryName = factoryName;
        this.factoryNameVN = factoryNameVN;
        this.factoryAdressVN = factoryAdressVN;
        this.newPrice = newPrice;
        this.changePrice = changePrice;
    }

    public String getWrdNumber() {
        return wrdNumber;
    }

    public void setWrdNumber(String wrdNumber) {
        this.wrdNumber = wrdNumber;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getRfidTagID() {
        return rfidTagID;
    }

    public void setRfidTagID(String rfidTagID) {
        this.rfidTagID = rfidTagID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getStyleColorSize() {
        return styleColorSize;
    }

    public void setStyleColorSize(String styleColorSize) {
        this.styleColorSize = styleColorSize;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getQuantityOrg() {
        return quantityOrg;
    }

    public void setQuantityOrg(int quantityOrg) {
        this.quantityOrg = quantityOrg;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getDescriptionVN() {
        return descriptionVN;
    }

    public void setDescriptionVN(String descriptionVN) {
        this.descriptionVN = descriptionVN;
    }

    public String getLifeStylePerformance() {
        return lifeStylePerformance;
    }

    public void setLifeStylePerformance(String lifeStylePerformance) {
        this.lifeStylePerformance = lifeStylePerformance;
    }

    public String getDiv() {
        return div;
    }

    public void setDiv(String div) {
        this.div = div;
    }

    public String getOutsole() {
        return outsole;
    }

    public void setOutsole(String outsole) {
        this.outsole = outsole;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProductline() {
        return productline;
    }

    public void setProductline(String productline) {
        this.productline = productline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getExWorkUSD() {
        return exWorkUSD;
    }

    public void setExWorkUSD(Double exWorkUSD) {
        this.exWorkUSD = exWorkUSD;
    }

    public Double getExWorkVND() {
        return exWorkVND;
    }

    public void setExWorkVND(Double exWorkVND) {
        this.exWorkVND = exWorkVND;
    }

    public Double getAmountVND() {
        return amountVND;
    }

    public void setAmountVND(Double amountVND) {
        this.amountVND = amountVND;
    }

    public Double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(Double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getCoo() {
        return coo;
    }

    public void setCoo(String coo) {
        this.coo = coo;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getDateOfInvoice() {
        return dateOfInvoice;
    }

    public void setDateOfInvoice(String dateOfInvoice) {
        this.dateOfInvoice = dateOfInvoice;
    }

    public String getSaleContract() {
        return saleContract;
    }

    public void setSaleContract(String saleContract) {
        this.saleContract = saleContract;
    }

    public String getShipment() {
        return shipment;
    }

    public void setShipment(String shipment) {
        this.shipment = shipment;
    }

    public String getFactoryAdress() {
        return factoryAdress;
    }

    public void setFactoryAdress(String factoryAdress) {
        this.factoryAdress = factoryAdress;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getFactoryNameVN() {
        return factoryNameVN;
    }

    public void setFactoryNameVN(String factoryNameVN) {
        this.factoryNameVN = factoryNameVN;
    }

    public String getFactoryAdressVN() {
        return factoryAdressVN;
    }

    public void setFactoryAdressVN(String factoryAdressVN) {
        this.factoryAdressVN = factoryAdressVN;
    }

    public Double getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(Double newPrice) {
        this.newPrice = newPrice;
    }

    public String getChangePrice() {
        return changePrice;
    }

    public void setChangePrice(String changePrice) {
        this.changePrice = changePrice;
    }
}
