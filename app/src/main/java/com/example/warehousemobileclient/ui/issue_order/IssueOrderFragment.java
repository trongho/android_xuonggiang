package com.example.warehousemobileclient.ui.issue_order;

import androidx.core.widget.ContentLoadingProgressBar;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WIDataHeaderAdapter;
import com.example.warehousemobileclient.adapter.WRDataHeaderAdapter2;
import com.example.warehousemobileclient.model.WIDataHeader;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.example.warehousemobileclient.ui.warehouse_receipt.ReceiptDataDetail;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueOrderFragment extends Fragment {
    RecyclerView recyclerView;
    private WIDataHeaderAdapter adapter;
    ArrayList<WIDataHeader> arrayList=new ArrayList<>();

    private IssueOrderViewModel mViewModel;

    public static IssueOrderFragment newInstance() {
        return new IssueOrderFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.issue_order_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(IssueOrderViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        ContentLoadingProgressBar progress = view.findViewById(R.id.progress);
        recyclerView = view.findViewById(R.id.categories_recycler_view);

        progress.show();

        IWarehouseApi iWarehouseApi = RetrofitService.getService();
        Call<List<WIDataHeader>> callback =iWarehouseApi.getWIDataHeaders();
        callback.enqueue(new Callback<List<WIDataHeader>>() {
            @Override
            public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                arrayList=(ArrayList<WIDataHeader>)response.body();
                setAdapter();
                progress.hide();
            }

            @Override
            public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                Toast.makeText(getContext(),"Fetch data fail",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter(){
        adapter=new WIDataHeaderAdapter(getContext(), arrayList, new WIDataHeaderAdapter.ItemClickListener() {
            @Override
            public void onClick(WIDataHeader wiDataHeader) {
                Bundle bundle = new Bundle();
                bundle.putString(IssueOrderDetailFragment.WIDNumber,wiDataHeader.getWidNumber());
                Navigation.findNavController(getActivity(),R.id.nav_host_fragment_activity_main).navigate(R.id.issueOrderDetaildest, bundle,getNavOptions());
            }
        });
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment_activity_main).popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected NavOptions getNavOptions() {
        NavOptions navOptions = new NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in_right)
                .setExitAnim(R.anim.slide_out_left)
                .setPopEnterAnim(R.anim.slide_in_left)
                .setPopExitAnim(R.anim.slide_out_right)
                .build();
        return navOptions;
    }
}