package com.example.warehousemobileclient.ui.sort_goods_data;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.warehousemobileclient.R;

public class SortGoodsDataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_goods_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Xếp hàng");
    }
}