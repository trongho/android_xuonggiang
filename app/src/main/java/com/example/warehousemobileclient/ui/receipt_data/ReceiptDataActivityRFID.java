package com.example.warehousemobileclient.ui.receipt_data;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WRDataGeneralRFIDAdapter;
import com.example.warehousemobileclient.helper.RFIDHandler;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.zebra.rfid.api3.TagData;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptDataActivityRFID extends AppCompatActivity implements RFIDHandler.ResponseHandlerInterface {
    public TextView statusTextViewRFID = null;
    private TextView textrfid;
    private TextView testStatus;

    RFIDHandler rfidHandler;
    final static String TAG = "RFID_SAMPLE";

    private RecyclerView recyclerView = null;
    private EditText edtWRDNumber = null;
    private EditText edtRFIDTagID = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantityOrg = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWRDNumber = null;
    private Button btnClearGoodsID = null;
    private Button btnClearQuantity = null;
    private WRDataGeneralRFIDAdapter adapter;
    ArrayList<WRDataGeneral> wrDataGenerals;
    ArrayList<WRDataGeneral> arrayListByRFIDTagID;
    ArrayList<WRDataHeader> wrDataHeaders;
    ArrayList<String> tagDatas;
    WRDataHeader wrDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Boolean isTagIDExist = false;
    Boolean isTagIdDuplicate = false;
    Double totalGoods = 0.0;
    Double totalQuantity = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_data2);
        initComponent();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Nhập hàng");
        // UI
        statusTextViewRFID = findViewById(R.id.textStatus);

        // RFID Handler
        rfidHandler = new RFIDHandler();
        rfidHandler.onCreate(this);

        edtWRDNumber.requestFocus();

        wrDataHeaders = new ArrayList<>();
        wrDataGenerals = new ArrayList<>();
        arrayListByRFIDTagID = new ArrayList<>();
        tagDatas = new ArrayList<>();
//        arrayListByIDCode = new ArrayList<>();

        edtWRDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWRDNumber.requestFocus();
                    return;
                }
                if (editable.length() >= 11) {
                    Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(edtWRDNumber.getText().toString().trim());
                    callback.enqueue(new Callback<List<WRDataHeader>>() {
                        @Override
                        public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                            if (response.isSuccessful()) {
                                wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                                if (wrDataHeaders.size() > 0) {
                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
                                    wrDataHeader = wrDataHeaders.get(0);
                                    fetchWRDataGeneralData(edtWRDNumber.getText().toString());
                                    hideSoftKeyBoard();
//                                    edtReferenceNumber.setText(wrDataHeader.getReferenceNumber());
                                    edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
                                    edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                                    edtRFIDTagID.setText("");
                                    edtQuantity.setText("");
                                    edtRFIDTagID.requestFocus();
                                } else {
                                    alertError(getApplicationContext(), "Không tìm thấy chi tiết dữ liệu");
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
//                    fetchWRDataDetailData(edtWRDNumber.getText().toString());
                }
            }
        });

        edtRFIDTagID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }

            }
        });

        btnClearWRDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtWRDNumber.setText("");
//                edtReferenceNumber.setText("");
            }
        });
        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtRFIDTagID.setText("");
            }
        });
        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtWRDNumber = findViewById(R.id.edtWRDNumber);
//        edtReferenceNumber = findViewById(R.id.edtReferenceNumber);
        edtRFIDTagID = findViewById(R.id.edtTagID);
//        edtIDCode = findViewById(R.id.edtIDCode);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearWRDNumber = findViewById(R.id.btn_clear_wrdnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
//        btnClearIDCode = findViewById(R.id.btn_clear_idcode);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantityOrg = findViewById(R.id.edtTotalQuantityOrg);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
    }

    @Override
    protected void onPause() {
        super.onPause();
        rfidHandler.onPause();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        String status = rfidHandler.onResume();
        statusTextViewRFID.setText(status);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        rfidHandler.onDestroy();
    }


    @Override
    public void handleTagdata(TagData[] tagData) {
        final StringBuilder sb = new StringBuilder();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int index = 0; index < tagData.length; index++) {
                    autoProcess(tagData[index].getTagID());
                }
            }
        });
    }

    @Override
    public void handleTriggerPress(boolean pressed) {
        if (pressed) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    edtRFIDTagID.setText("");
                }
            });
            rfidHandler.performInventory();
        } else
            rfidHandler.stopInventory();
    }

    private WRDataHeader fetchWRDataHeaderData(String wrdNumber) {
        Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(wrdNumber);
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                if (response.isSuccessful()) {
                    wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                    if (wrDataHeaders.size() > 0) {
                        wrDataHeader = wrDataHeaders.get(0);
                        hideSoftKeyBoard();
                        edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
                        edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                        edtTotalGoodsOrg.setText(String.format("%.0f", wrDataHeader.getTotalGoodsOrg()));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataHeader;
    }

    private ArrayList<WRDataGeneral> fetchWRDataGeneralData(String wrdNumber) {
        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralById(wrdNumber);
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wrDataGenerals = (ArrayList<WRDataGeneral>) response.body();
                    if (wrDataGenerals.size() > 0) {

                        totalGoods = 0.0;
                        getTotalGoods(wrDataGenerals);
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                    }
                    setAdapter(wrDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataGenerals;
    }

    private void putWRDataHeader(String wRDNumber, WRDataHeader wrDataHeader) {
        Call<WRDataHeader> callback = iWarehouseApi.updateWRDataHeader(wRDNumber, wrDataHeader);
        callback.enqueue(new Callback<WRDataHeader>() {
            @Override
            public void onResponse(Call<WRDataHeader> call, Response<WRDataHeader> response) {
                if (response.isSuccessful()) {
                    fetchWRDataHeaderData(wRDNumber);
                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                }
            }

            @Override
            public void onFailure(Call<WRDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdataheader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWRDataGeneral(String wRDNumber, String RFIDTagID, int ordinal, WRDataGeneral wrDataGeneral) {
        Call<WRDataGeneral> callback = iWarehouseApi.updateWRDataGeneral(wRDNumber, RFIDTagID, ordinal, wrDataGeneral);
        callback.enqueue(new Callback<WRDataGeneral>() {
            @Override
            public void onResponse(Call<WRDataGeneral> call, Response<WRDataGeneral> response) {
                if (response.isSuccessful()) {
                    fetchWRDataGeneralData(wRDNumber);
                    edtTotalGoods.setText(String.format("%.0f", totalGoods));
                }
            }

            @Override
            public void onFailure(Call<WRDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getTotalQuantity(ArrayList<WRDataGeneral> wrDataGenerals) {
        totalQuantity = 0.0;
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.size() > 0 && wrDataGenerals.get(i).getQuantity() > 0) {
                totalQuantity += wrDataGenerals.get(i).getQuantity();
            }
        }
    }

    private void getTotalGoods(ArrayList<WRDataGeneral> wrDataGenerals) {
        totalGoods = 0.0;
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity()> 0) {
                totalGoods += 1;
            }
        }
    }

    private void checkExistRFIDTagID(String RFIDtagID, ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (RFIDtagID.equalsIgnoreCase(wrDataGenerals.get(i).getBarcode())) {
                isTagIDExist = true;
                break;
            }
        }
    }

    private void checkRFIDTagIDDuplicate(ArrayList<WRDataGeneral> wrDataGenerals, String RFIDTagID) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getBarcode().equalsIgnoreCase(RFIDTagID)) {
                isTagIdDuplicate = true;
                break;
            }
        }
    }


    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void setAdapter(ArrayList<WRDataGeneral> wrDataGenerals) {
        adapter = new WRDataGeneralRFIDAdapter(getApplicationContext(), wrDataGenerals, new WRDataGeneralRFIDAdapter.ItemClickListener() {
            @Override
            public void onClick(WRDataGeneral wrDataGeneral) {
                edtRFIDTagID.setText(wrDataGeneral.getBarcode());
                edtQuantity.setText(String.format("%.0f", wrDataGeneral.getQuantity()));
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void autoProcess(String tagData) {
        isTagIDExist = false;
        checkExistRFIDTagID(tagData, wrDataGenerals);
        if (isTagIDExist == true) {
            isTagIDExist = false;
        } else {
            return;
        }

        getTotalQuantity(wrDataGenerals);
        getTotalGoods(wrDataGenerals);

        String barcode=tagData.substring(0,11);


        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralByBarcode(edtWRDNumber.getText().toString(), barcode);
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    arrayListByRFIDTagID = (ArrayList<WRDataGeneral>) response.body();
                    if (arrayListByRFIDTagID.size() > 0) {
                        WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                        WRDataGeneral wrDataGeneralUpdate = arrayListByRFIDTagID.get(0);

                        isTagIdDuplicate = false;
                        checkRFIDTagIDDuplicate(arrayListByRFIDTagID, tagData);
                        if (isTagIdDuplicate == false) {
                            wrDataGeneralUpdate.setQuantity(wrDataGeneralUpdate.getQuantityOrg());
                            wrDataHeaderUpdate.setTotalQuantity(totalQuantity + wrDataGeneralUpdate.getQuantityOrg());
//                            wrDataGeneralUpdate.setTotalGoods(totalGoods + 1);
//                            wrDataGeneralUpdate.setTotalQuantity(totalQuantity + wrDataGeneralUpdate.getQuantityOrg());
                            wrDataHeaderUpdate.setTotalGoods(wrDataHeaderUpdate.getTotalGoods()+1);
                            try {
                                AssetFileDescriptor afd = null;
                                afd = getAssets().openFd("beep.mp3");
                                MediaPlayer player = new MediaPlayer();
                                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                                player.prepare();
                                player.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            wrDataHeaderUpdate.setTotalQuantity(totalQuantity);
//                            wrDataGeneralUpdate.setTotalGoods(totalGoods);
//                            wrDataGeneralUpdate.setTotalQuantity(totalQuantity);
                        }
                        wrDataGeneralUpdate.setWrdNumber(edtWRDNumber.getText().toString());
//                        wrDataGeneralUpdate.setGoodsID(wrDataGeneralUpdate.getGoodsID());
                        wrDataGeneralUpdate.setOrdinal(arrayListByRFIDTagID.get(0).getOrdinal());
                        wrDataGeneralUpdate.setInputDate(simpleDateFormat.format(Calendar.getInstance().getTime()));


                        putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                        putWRDataGeneral(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getBarcode(), wrDataGeneralUpdate.getOrdinal(), wrDataGeneralUpdate);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "failure" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void process(String tagData) {
        if (TextUtils.isEmpty(edtRFIDTagID.getText().toString())) {
            alertError(getApplicationContext(), "Tag id trống");
            edtRFIDTagID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
            alertError(getApplicationContext(), "Số lượng trống");
            edtQuantity.requestFocus();
            return;
        }

        isTagIDExist = false;
        checkExistRFIDTagID(tagData, wrDataGenerals);
        if (isTagIDExist == true) {
            isTagIDExist = false;
        } else {
            return;
        }

        totalQuantity = 0.0;
        totalGoods = 0.0;
        getTotalQuantity(wrDataGenerals);
        getTotalGoods(wrDataGenerals);

        String barcode=tagData.substring(0,11);

        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralByBarcode(edtWRDNumber.getText().toString(),barcode);
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    arrayListByRFIDTagID = (ArrayList<WRDataGeneral>) response.body();
                    if (arrayListByRFIDTagID.size() > 0) {
                        WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                        WRDataGeneral wrDataGeneralUpdate = arrayListByRFIDTagID.get(0);

                        isTagIdDuplicate = false;
                        checkRFIDTagIDDuplicate(arrayListByRFIDTagID, tagData);
                        if (isTagIdDuplicate == false) {
                            wrDataHeaderUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                            wrDataHeaderUpdate.setTotalGoods(wrDataHeaderUpdate.getTotalGoods()+1);
//                            wrDataGeneralUpdate.setTotalGoods(totalGoods + 1);
//                            wrDataGeneralUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        } else {
                            wrDataHeaderUpdate.setTotalQuantity(totalQuantity - wrDataGeneralUpdate.getQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
//                            wrDataGeneralUpdate.setTotalGoods(totalGoods);
//                            wrDataGeneralUpdate.setTotalQuantity(totalQuantity - wrDataGeneralUpdate.getQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
                        }
                        wrDataGeneralUpdate.setQuantity(Integer.parseInt(edtQuantity.getText().toString()));
                        wrDataGeneralUpdate.setWrdNumber(edtWRDNumber.getText().toString());
//                        wrDataGeneralUpdate.setGoodsID(wrDataGeneralUpdate.getGoodsID());
                        wrDataGeneralUpdate.setOrdinal(arrayListByRFIDTagID.get(0).getOrdinal());

                        putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                        putWRDataGeneral(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getBarcode(), wrDataGeneralUpdate.getOrdinal(), wrDataGeneralUpdate);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "failure" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_wrdatadetail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<WRDataGeneral> allList = wrDataGenerals;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_yet) {
            ArrayList<WRDataGeneral> notYetList = getNotYetList(wrDataGenerals);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_enought_yet) {
            ArrayList<WRDataGeneral> enoughtYetLish = getEnoughtYetList(wrDataGenerals);
            setAdapter(enoughtYetLish);
            return true;
        }
        if (id == R.id.action_enought) {
            ArrayList<WRDataGeneral> enoughtList = getEnoughtList(wrDataGenerals);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_over) {
            ArrayList<WRDataGeneral> overList = getOvertList(wrDataGenerals);
            setAdapter(overList);
            return true;
        }
        if (id == R.id.action_save) {
            process(edtRFIDTagID.getText().toString());
            return true;
        }
        if (id == R.id.action_approve) {
            if (wrDataHeader != null) {
                WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                wrDataHeaderUpdate.setHandlingStatusID("2");
                wrDataHeaderUpdate.setHandlingStatusName("Duyệt mức 2");
                putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                alertSuccess(getApplicationContext(), "Duyệt mức 2 nhập kho thành công");
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<WRDataGeneral> getNotYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() == 0) {
                notYetList.add(wrDataGenerals.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WRDataGeneral> getEnoughtYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughYettList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity() < wrDataGenerals.get(i).getQuantityOrg()) {
                enoughYettList.add(wrDataGenerals.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WRDataGeneral> getEnoughtList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity() == wrDataGenerals.get(i).getQuantityOrg()) {
                enoughtList.add(wrDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WRDataGeneral> getOvertList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> overList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > wrDataGenerals.get(i).getQuantityOrg()) {
                overList.add(wrDataGenerals.get(i));
            }
        }
        return overList;
    }
}