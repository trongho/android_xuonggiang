package com.example.warehousemobileclient.ui.got_goods_data;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.warehousemobileclient.R;

public class GotGoodsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_got_goods);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Lấy hàng");
    }
}