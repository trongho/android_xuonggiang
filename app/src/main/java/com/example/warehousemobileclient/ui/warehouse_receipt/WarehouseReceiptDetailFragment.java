package com.example.warehousemobileclient.ui.warehouse_receipt;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WRDataGeneralBarcodeAdapter;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WarehouseReceiptDetailFragment extends Fragment {
    private TextView tvWRDNumber = null;
    private TextView tvWRDReferenceNumber = null;
    private TextView tvWRDDate = null;
    private TextView tvWRDHandlingStatus = null;
    private TextView tvQuantityOrg = null;
    private TextView tvQuantity = null;
    private TextView tvWRDataGeneral = null;
    public static final String WRDNumber = "wrdnumber";
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    WRDataHeader wrDataHeader;
    ArrayList<WRDataGeneral> wrDataGenerals;
    private RecyclerView recyclerView = null;
    private WRDataGeneralBarcodeAdapter adapter;
    String wrdNumber = "";

    public WarehouseReceiptDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_warehouse_receipt_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
        initComponent(view);

        wrDataGenerals = new ArrayList<>();
        wrDataHeader=new WRDataHeader();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            wrdNumber = getArguments().getString(WRDNumber);
        }

        fetchWRDHeaderByWRDNumber(wrdNumber);
        fetchWRDataGeneral(wrdNumber);


        tvWRDataGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recyclerView.getVisibility() == View.VISIBLE) {
                    recyclerView.setVisibility(View.INVISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void initComponent(View view) {
        tvWRDNumber = view.findViewById(R.id.tvWRDNumer);
        tvWRDReferenceNumber = view.findViewById(R.id.tvReferenceNumber);
        tvWRDDate = view.findViewById(R.id.tvWRDDate);
        tvWRDHandlingStatus = view.findViewById(R.id.tvHandlingStatus);
        tvQuantityOrg = view.findViewById(R.id.tvTotalQuantityOrg);
        tvQuantity = view.findViewById(R.id.tvTotalQuantity);
        tvWRDataGeneral = view.findViewById(R.id.tvWRDataGeneral);
        recyclerView = view.findViewById(R.id.categories_recycler_view);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment_activity_main).popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private WRDataHeader fetchWRDHeaderByWRDNumber(String wRDNumer) {
        Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(wRDNumer);
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                if (response.isSuccessful()) {
                    wrDataHeader = response.body().get(0);
                    tvWRDNumber.setText(wrDataHeader.getWrdNumber());
                    tvWRDReferenceNumber.setText(wrDataHeader.getReferenceNumber());
                    tvWRDDate.setText(wrDataHeader.getWrdDate());
                    tvWRDHandlingStatus.setText(wrDataHeader.getHandlingStatusName());
                    tvQuantityOrg.setText(wrDataHeader.getTotalQuantityOrg() + "");
                    tvQuantity.setText(wrDataHeader.getTotalQuantity() + "");
                    Toast.makeText(getContext(), "Fetch success!!!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Not found!!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getContext(), "fetch fail!!!", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataHeader;
    }

    private ArrayList<WRDataGeneral> fetchWRDataGeneral(String wrdNumber) {
        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralById(wrdNumber);
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wrDataGenerals = (ArrayList<WRDataGeneral>) response.body();
                    setAdapter(wrDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataGenerals;
    }

    private void setAdapter(ArrayList<WRDataGeneral> wrDataGenerals) {
        adapter = new WRDataGeneralBarcodeAdapter(getContext(), wrDataGenerals, new WRDataGeneralBarcodeAdapter.ItemClickListener() {
            @Override
            public void onClick(WRDataGeneral wrDataGeneral) {

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}