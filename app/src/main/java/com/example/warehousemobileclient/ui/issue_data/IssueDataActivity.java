package com.example.warehousemobileclient.ui.issue_data;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WIDataGeneralAdapter;
import com.example.warehousemobileclient.model.WIDataGeneral;
import com.example.warehousemobileclient.model.WIDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueDataActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    private RecyclerView recyclerView = null;
    private EditText edtWIDNumber = null;
    //    private EditText edtReferenceNumber = null;
    private EditText edtGoodsID = null;
    //    private EditText edtIDCode = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantityOrg = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWRDNumber = null;
    private Button btnClearGoodsID = null;
    //    private Button btnClearIDCode = null;
    private Button btnClearQuantity = null;
    private WIDataGeneralAdapter adapter;
    //    ArrayList<WIDataDetail> wiDataDetails;
    ArrayList<WIDataGeneral> wiDataGenerals;
    //    ArrayList<WIDataDetail> arrayListByIDCode;
    ArrayList<WIDataGeneral> arrayListByGoodsID;
    ArrayList<WIDataHeader> wiDataHeaders;
    WIDataHeader wiDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    Boolean isGoodsIDExist = false;
    //    Boolean isGoodsIDAndIDCodeExist = false;
//    Boolean isGoodsIDAndIDCodeScanned = false;
    Boolean isGoodsIdDuplicate = false;
    Double totalGoods = 0.0;
    Double totalQuantity = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Xuất hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();
        wiDataHeaders = new ArrayList<>();
        wiDataGenerals = new ArrayList<>();
        arrayListByGoodsID = new ArrayList<>();
//        arrayListByIDCode = new ArrayList<>();

        edtWIDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWIDNumber.requestFocus();
                    return;
                }
                if (editable.length() >= 7) {
                    Call<List<WIDataHeader>> callback = iWarehouseApi.getWIDataHeader(edtWIDNumber.getText().toString());
                    callback.enqueue(new Callback<List<WIDataHeader>>() {
                        @Override
                        public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                            if (response.isSuccessful()) {
                                wiDataHeaders = (ArrayList<WIDataHeader>) response.body();
                                if (wiDataHeaders.size() > 0) {
                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
                                    wiDataHeader = wiDataHeaders.get(0);
                                    fetchWIDataGeneralData(edtWIDNumber.getText().toString());
                                    hideSoftKeyBoard();
//                                    edtReferenceNumber.setText(wiDataHeader.getReferenceNumber());
                                    edtTotalQuantityOrg.setText(String.format("%.0f", wiDataHeader.getTotalQuantityOrg()));
                                    edtTotalQuantity.setText(String.format("%.0f", wiDataHeader.getTotalQuantity()));
                                    edtGoodsID.requestFocus();
                                    ;
                                } else {
                                    alertError(getApplicationContext(), "Không tìm thấy chi tiết dữ liệu");
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        edtGoodsID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWIDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance Number");
                    edtWIDNumber.requestFocus();
                    return;
                }
            }
        });

//        edtIDCode.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                String wrdNumber = edtWIDNumber.getText().toString();
//                String goodsID = edtGoodsID.getText().toString();
//                String idCode = edtIDCode.getText().toString();
//                if (TextUtils.isEmpty(edtWIDNumber.getText().toString())) {
//                    alertError(getApplicationContext(), "Nhập số phiếu");
//                    edtWIDNumber.requestFocus();
//                    return;
//                }
//                if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
//                    alertError(getApplicationContext(), "Scan mã hàng");
//                    edtGoodsID.requestFocus();
//                    return;
//                }
//                if (TextUtils.isEmpty(edtIDCode.getText().toString())) {
//                    alertError(getApplicationContext(), "Scan mã định danh");
//                    edtIDCode.requestFocus();
//                    return;
//                }
////                if (editable.length() >= 3) {
////                    fetchWIDataDetailByIdCode(wrdNumber, goodsID, idCode);
////                    if (arrayListByIDCode.size() > 0) {
////                        edtQuantity.requestFocus();
////                    }
////                }
//            }
//        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWIDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance Number");
                    edtWIDNumber.requestFocus();
                    return;
                }
            }
        });

        btnClearWRDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtWIDNumber.setText("");
//                edtReferenceNumber.setText("");
            }
        });
        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtGoodsID.setText("");
            }
        });
//        btnClearIDCode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                edtIDCode.setText("");
//            }
//        });
        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtWIDNumber = findViewById(R.id.edtWRDNumber);
//        edtReferenceNumber = findViewById(R.id.edtReferenceNumber);
        edtGoodsID = findViewById(R.id.edtGoodsID);
//        edtIDCode = findViewById(R.id.edtIDCode);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearWRDNumber = findViewById(R.id.btn_clear_wrdnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
//        btnClearIDCode = findViewById(R.id.btn_clear_idcode);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantityOrg = findViewById(R.id.edtTotalQuantityOrg);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
    }

    private WIDataHeader fetchWIDataHeaderData(String widNumber) {
        Call<List<WIDataHeader>> callback = iWarehouseApi.getWIDataHeader(widNumber);
        callback.enqueue(new Callback<List<WIDataHeader>>() {
            @Override
            public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                if (response.isSuccessful()) {
                    wiDataHeaders = (ArrayList<WIDataHeader>) response.body();
                    if (wiDataHeaders.size() > 0) {
                        wiDataHeader = wiDataHeaders.get(0);
                        hideSoftKeyBoard();
//                        edtReferenceNumber.setText(wiDataHeader.getReferenceNumber());
                        edtTotalQuantityOrg.setText(String.format("%.0f", wiDataHeader.getTotalQuantityOrg()));
                        edtTotalQuantity.setText(String.format("%.0f", wiDataHeader.getTotalQuantity()));
                        edtGoodsID.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataHeader;
    }


//    private ArrayList<WIDataDetail> fetchWIDataDetailData(String widNumber) {
//        Call<List<WIDataDetail>> callback = iWarehouseApi.getWIDataDetailById(widNumber);
//        callback.enqueue(new Callback<List<WIDataDetail>>() {
//            @Override
//            public void onResponse(Call<List<WIDataDetail>> call, Response<List<WIDataDetail>> response) {
//                if (response.isSuccessful()) {
//                    wiDataDetails = (ArrayList<WIDataDetail>) response.body();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WIDataDetail>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return wiDataDetails;
//    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralData(String widNumber) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.getWIDataGeneralById(widNumber);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                    if (wiDataGenerals.size() > 0) {
                        edtTotalGoodsOrg.setText(String.format("%.0f", wiDataGenerals.get(0).getTotalGoodsOrg()));
                        totalGoods = 0.0;
                        getTotalGoods(wiDataGenerals);
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                    }
                    setAdapter(wiDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

//    private ArrayList<WIDataGeneral> fetchWIDataGeneralByGoodsID(String widNumber, String goodsId) {
//        Call<List<WIDataGeneral>> callback = iWarehouseApi.getWIDataGeneralByGoods(widNumber, goodsId);
//        callback.enqueue(new Callback<List<WIDataGeneral>>() {
//            @Override
//            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
//                if (response.isSuccessful()) {
//                    arrayListByGoodsID = (ArrayList<WIDataGeneral>) response.body();
//                    if (arrayListByGoodsID.size() > 0) {
//                        Toast.makeText(getApplicationContext(), "succes!!! " + arrayListByGoodsID.get(0).getGoodsID() + "", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    alertError(getApplicationContext(), "Không tìm thấy mã định danh theo mã hàng");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return arrayListByGoodsID;
//    }

//    private ArrayList<WIDataDetail> fetchWIDataDetailByIdCode(String widNumber, String goodsId, String idCode) {
//        Call<List<WIDataDetail>> callback = iWarehouseApi.getWIDataDetailByIdCode(widNumber, goodsId, idCode);
//        callback.enqueue(new Callback<List<WIDataDetail>>() {
//            @Override
//            public void onResponse(Call<List<WIDataDetail>> call, Response<List<WIDataDetail>> response) {
//                if (response.isSuccessful()) {
//                    arrayListByIDCode = new ArrayList<>();
//                    arrayListByIDCode = (ArrayList<WIDataDetail>) response.body();
//                    if (arrayListByIDCode.size() > 0) {
//                        Toast.makeText(getApplicationContext(), "succes!!! " + arrayListByIDCode.get(0).getGoodsID() + "-" + arrayListByIDCode.get(0).getIdCode(), Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    alertError(getApplicationContext(), "Không tìm thấy mã định danh theo mã hàng");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WIDataDetail>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return arrayListByIDCode;
//    }

    private void putWIDataHeader(String wIDNumber, WIDataHeader wiDataHeader) {
        Call<WIDataHeader> callback = iWarehouseApi.updateWIDataHeader(wIDNumber, wiDataHeader);
        callback.enqueue(new Callback<WIDataHeader>() {
            @Override
            public void onResponse(Call<WIDataHeader> call, Response<WIDataHeader> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWIDataHeaderData(wIDNumber);
                } else {
                    Toast.makeText(getApplicationContext(), "put wrdataheader" + response.code() + "", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<WIDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdataheader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWIDataGeneral(String wIDNumber, String goodsID, int ordinal, WIDataGeneral wiDataGeneral) {
        Call<WIDataGeneral> callback = iWarehouseApi.updateWIDataGeneral(wIDNumber, goodsID, ordinal, wiDataGeneral);
        callback.enqueue(new Callback<WIDataGeneral>() {
            @Override
            public void onResponse(Call<WIDataGeneral> call, Response<WIDataGeneral> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWIDataGeneralData(wIDNumber);
                    getTotalQuantity(wiDataGenerals);
                    getTotalGoods(wiDataGenerals);
                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                    edtTotalGoods.setText(String.format("%.0f", totalGoods));
                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WIDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void putWIDataDetail(String wIDNumber, String goodsID, int ordinal, WIDataDetail wiDataDetail) {
//        Call<WIDataDetail> callback = iWarehouseApi.updateWIDataDetail(wIDNumber, goodsID, ordinal, wiDataDetail);
//        callback.enqueue(new Callback<WIDataDetail>() {
//            @Override
//            public void onResponse(Call<WIDataDetail> call, Response<WIDataDetail> response) {
//                if (response.isSuccessful()) {
//                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
//                } else {
//                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<WIDataDetail> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void getTotalQuantity(ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.size() > 0 && wiDataGenerals.get(i).getQuantity() > 0) {
                totalQuantity += wiDataGenerals.get(i).getQuantity();
            }
        }
    }

    private void getTotalGoods(ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity().doubleValue() > 0) {
                totalGoods += 1;
            }
        }
    }

    private Boolean checkGoodsIDDuplicate(ArrayList<WIDataGeneral> wiDataGenerals, String goodsID) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() > 0 && wiDataGenerals.get(i).getGoodsID().equalsIgnoreCase(goodsID)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }

    private Boolean checkExistGoodsID(String goodsID, ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (goodsID.equalsIgnoreCase(wiDataGenerals.get(i).getGoodsID())) {
                isGoodsIDExist = true;
                break;
            }
        }
        return isGoodsIDExist;
    }

//    private Boolean checkExistGoodsIDAndIDCode(String goodsID, String idCode, ArrayList<WIDataDetail> arrayList) {
//        for (int i = 0; i < arrayList.size(); i++) {
//            if (goodsID.equalsIgnoreCase(arrayList.get(i).getGoodsID()) && idCode.equalsIgnoreCase(arrayList.get(i).getIdCode())) {
//                isGoodsIDAndIDCodeExist = true;
//                break;
//            }
//        }
//        return isGoodsIDAndIDCodeExist;
//    }
//
//    private Boolean checkGoodsIDAndIDCodeScaned(String goodsID, String idCode, ArrayList<WIDataDetail> wiDataDetails) {
//        for (int i = 0; i < wiDataDetails.size(); i++) {
//            if (goodsID.equalsIgnoreCase(wiDataDetails.get(i).getGoodsID()) && idCode.equalsIgnoreCase(wiDataDetails.get(i).getIdCode())
//                    && wiDataDetails.get(i).getQuantity().doubleValue() > 0) {
//                isGoodsIDAndIDCodeScanned = true;
//                break;
//            }
//        }
//        return isGoodsIDAndIDCodeScanned;
//    }

    private ArrayList<WIDataGeneral> getNotYetList(ArrayList<WIDataGeneral> wiDataGenerals) {
        ArrayList<WIDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() == 0) {
                notYetList.add(wiDataGenerals.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WIDataGeneral> getEnoughtYetList(ArrayList<WIDataGeneral> wiDataGenerals) {
        ArrayList<WIDataGeneral> enoughYettList = new ArrayList<>();
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() > 0 && wiDataGenerals.get(i).getQuantity() < wiDataGenerals.get(i).getQuantityOrg()) {
                enoughYettList.add(wiDataGenerals.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WIDataGeneral> getEnoughtList(ArrayList<WIDataGeneral> wiDataGenerals) {
        ArrayList<WIDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() > 0 && wiDataGenerals.get(i).getQuantity().doubleValue() == wiDataGenerals.get(i).getQuantityOrg().doubleValue()) {
                enoughtList.add(wiDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WIDataGeneral> getOvertList(ArrayList<WIDataGeneral> wiDataGenerals) {
        ArrayList<WIDataGeneral> overList = new ArrayList<>();
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() > wiDataGenerals.get(i).getQuantityOrg()) {
                overList.add(wiDataGenerals.get(i));
            }
        }
        return overList;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void setAdapter(ArrayList<WIDataGeneral> wiDataGenerals) {
        adapter = new WIDataGeneralAdapter(getApplicationContext(), wiDataGenerals, new WIDataGeneralAdapter.ItemClickListener() {
            @Override
            public void onClick(WIDataGeneral wiDataGeneral) {
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_wrdatadetail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<WIDataGeneral> allList = wiDataGenerals;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_yet) {
            ArrayList<WIDataGeneral> notYetList = getNotYetList(wiDataGenerals);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_enought_yet) {
            ArrayList<WIDataGeneral> enoughtYetLish = getEnoughtYetList(wiDataGenerals);
            setAdapter(enoughtYetLish);
            return true;
        }
        if (id == R.id.action_enought) {
            ArrayList<WIDataGeneral> enoughtList = getEnoughtList(wiDataGenerals);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_over) {
            ArrayList<WIDataGeneral> overList = getOvertList(wiDataGenerals);
            setAdapter(overList);
            return true;
        }
        if (id == R.id.action_save) {
            process();
            return true;
        }
        if (id == R.id.action_approve) {
            if (wiDataHeader != null) {
                WIDataHeader wiDataHeaderUpdate = wiDataHeader;
                wiDataHeaderUpdate.setHandlingStatusID("2");
                wiDataHeaderUpdate.setHandlingStatusName("Duyệt mức 2");
                putWIDataHeader(edtWIDNumber.getText().toString(), wiDataHeaderUpdate);
                alertSuccess(getApplicationContext(), "Duyệt mức 2 xuất kho thành công");
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            for (ScanDataCollection.ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        StatusData.ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = Scanner.TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = Scanner.TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(EMDKManager.FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
//                    if (edtGoodsID.hasFocus()) {
//                        edtGoodsID.setText(result);
//                        if (isGoodsIDExist == true) {
//                            edtIDCode.requestFocus();
//                        }
//                    } else if (edtIDCode.hasFocus()) {
//                        edtIDCode.setText(result);
//                        if (isGoodsIDAndIDCodeExist == false) {
//                            edtQuantity.requestFocus();
//                        }
//                    } else if (edtQuantity.hasFocus()) {
//                        edtQuantity.setText(result);
//                    }
                    if (edtWIDNumber.hasFocus()) {
                        edtWIDNumber.setText(result);
                    }
                    String goodsID = "";
                    String quantity = "";
//                    String idCode = "";
                    int index1 = result.lastIndexOf("-");
                    if (index1 > 0) {
                        goodsID = result.substring(0, index1);
                        quantity = result.substring(index1 + 1, result.length());
//                        idCode = result.substring(index2 + 1, result.length());
                    }
                    edtGoodsID.setText(goodsID);
//                    edtIDCode.setText(idCode);
                    edtQuantity.setText(quantity);

                    process();
                }
            }
        });
    }

    private void process() {
        if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
            alertError(getApplicationContext(), "Nhập mã hàng");
            edtGoodsID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
            alertError(getApplicationContext(), "Nhập số lượng");
            edtQuantity.requestFocus();
            return;
        }

        isGoodsIDExist = false;
        checkExistGoodsID(edtGoodsID.getText().toString(), wiDataGenerals);
        if (isGoodsIDExist == true) {
            isGoodsIDExist = false;
        } else {
            alertError(getApplicationContext(), "Mã hàng không tồn tại");
            return;
        }

        totalQuantity = 0.0;
        totalGoods = 0.0;
        getTotalGoods(wiDataGenerals);
        getTotalQuantity(wiDataGenerals);

        Call<List<WIDataGeneral>> callback = iWarehouseApi.getWIDataGeneralByGoods(edtWIDNumber.getText().toString(), edtGoodsID.getText().toString());
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    arrayListByGoodsID = (ArrayList<WIDataGeneral>) response.body();
                    if (arrayListByGoodsID.size() > 0) {
                        WIDataGeneral wiDataGeneralUpdate = arrayListByGoodsID.get(0);
                        checkGoodsIDDuplicate(arrayListByGoodsID, edtGoodsID.getText().toString());
                        if (isGoodsIdDuplicate == false) {
                            wiDataGeneralUpdate.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                        } else {
                            wiDataGeneralUpdate.setQuantity(wiDataGeneralUpdate.getQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
                            isGoodsIdDuplicate = false;
                        }
                        wiDataGeneralUpdate.setWidNumber(edtWIDNumber.getText().toString());
                        wiDataGeneralUpdate.setGoodsID(edtGoodsID.getText().toString());
                        wiDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());
                        wiDataGeneralUpdate.setTotalGoods(totalGoods + 1);
                        wiDataGeneralUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        putWIDataGeneral(wiDataGeneralUpdate.getWidNumber(), wiDataGeneralUpdate.getGoodsID(), wiDataGeneralUpdate.getOrdinal(), wiDataGeneralUpdate);

                        WIDataHeader wiDataHeaderUpdate = wiDataHeader;
                        wiDataHeaderUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        putWIDataHeader(edtWIDNumber.getText().toString(), wiDataHeaderUpdate);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }
}