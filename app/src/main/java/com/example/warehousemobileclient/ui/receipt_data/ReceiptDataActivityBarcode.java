package com.example.warehousemobileclient.ui.receipt_data;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WRDataGeneralBarcodeAdapter;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptDataActivityBarcode extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    private RecyclerView recyclerView = null;
    private EditText edtWRDNumber = null;
    //    private EditText edtReferenceNumber = null;
    private EditText edtGoodsID = null;
    //    private EditText edtIDCode = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantityOrg = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWRDNumber = null;
    private Button btnClearGoodsID = null;
    //    private Button btnClearIDCode = null;
    private Button btnClearQuantity = null;
    private WRDataGeneralBarcodeAdapter adapter;
    //    ArrayList<WRDataDetail> wrDataDetails;
    ArrayList<WRDataGeneral> wrDataGenerals;
    //    ArrayList<WRDataDetail> arrayListByIDCode;
    ArrayList<WRDataGeneral> arrayListByGoodsID;
    ArrayList<WRDataHeader> wrDataHeaders;
    WRDataHeader wrDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    Boolean isGoodsIDExist = false;
    //    Boolean isGoodsIDAndIDCodeExist = false;
//    Boolean isGoodsIDAndIDCodeScanned = false;
    Boolean isGoodsIdDuplicate = false;
    Double totalGoods = 0.0;
    Double totalQuantity = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Nhập hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();

        edtWRDNumber.requestFocus();

        wrDataHeaders = new ArrayList<>();
        wrDataGenerals = new ArrayList<>();
        arrayListByGoodsID = new ArrayList<>();
//        arrayListByIDCode = new ArrayList<>();

        edtWRDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWRDNumber.requestFocus();
                    return;
                }
                if (editable.length() >= 11) {
                    Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(edtWRDNumber.getText().toString().trim());
                    callback.enqueue(new Callback<List<WRDataHeader>>() {
                        @Override
                        public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                            if (response.isSuccessful()) {
                                wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                                if (wrDataHeaders.size() > 0) {
                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
                                    wrDataHeader = wrDataHeaders.get(0);
                                    fetchWRDataGeneralData(edtWRDNumber.getText().toString());
                                    hideSoftKeyBoard();
//                                    edtReferenceNumber.setText(wrDataHeader.getReferenceNumber());
                                    edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
                                    edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                                    edtGoodsID.requestFocus();
                                } else {
                                    alertError(getApplicationContext(), "Không tìm thấy chi tiết dữ liệu");
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
//                    fetchWRDataDetailData(edtWRDNumber.getText().toString());
                }
            }
        });

        edtGoodsID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }

            }
        });

        btnClearWRDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtWRDNumber.setText("");
//                edtReferenceNumber.setText("");
            }
        });
        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtGoodsID.setText("");
            }
        });
//        btnClearIDCode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                edtIDCode.setText("");
//            }
//        });
        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtWRDNumber = findViewById(R.id.edtWRDNumber);
//        edtReferenceNumber = findViewById(R.id.edtReferenceNumber);
        edtGoodsID = findViewById(R.id.edtGoodsID);
//        edtIDCode = findViewById(R.id.edtIDCode);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearWRDNumber = findViewById(R.id.btn_clear_wrdnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
//        btnClearIDCode = findViewById(R.id.btn_clear_idcode);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantityOrg = findViewById(R.id.edtTotalQuantityOrg);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
    }

    private void clear() {
        edtGoodsID.setText("");
//        edtIDCode.setText("");
        edtQuantity.setText("");
    }

    private WRDataHeader fetchWRDataHeaderData(String wrdNumber) {
        Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(wrdNumber);
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                if (response.isSuccessful()) {
                    wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                    if (wrDataHeaders.size() > 0) {
                        wrDataHeader = wrDataHeaders.get(0);
                        hideSoftKeyBoard();
//                        edtReferenceNumber.setText(wrDataHeader.getReferenceNumber());
                        edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
                        edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                        edtTotalGoodsOrg.setText(String.format("%.0f",wrDataHeader.getTotalGoodsOrg()));
                        edtGoodsID.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataHeader;
    }

    private ArrayList<WRDataGeneral> fetchWRDataGeneralData(String wrdNumber) {
        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralById(wrdNumber);
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wrDataGenerals = (ArrayList<WRDataGeneral>) response.body();
                    if (wrDataGenerals.size() > 0) {

                        totalGoods = 0.0;
                        getTotalGoods(wrDataGenerals);
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                    }
                    setAdapter(wrDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataGenerals;
    }

//    private ArrayList<WRDataDetail> fetchWRDataDetailData(String wrdNumber) {
//        Call<List<WRDataDetail>> callback = iWarehouseApi.getWRDataDetailById(wrdNumber);
//        callback.enqueue(new Callback<List<WRDataDetail>>() {
//            @Override
//            public void onResponse(Call<List<WRDataDetail>> call, Response<List<WRDataDetail>> response) {
//                if (response.isSuccessful()) {
//                    wrDataDetails = (ArrayList<WRDataDetail>) response.body();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WRDataDetail>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return wrDataDetails;
//    }

//    private ArrayList<WRDataGeneral> fetchWRDataGeneralByGoodsID(String wrdNumber, String goodsId) {
//        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralByGoods(wrdNumber, goodsId);
//        callback.enqueue(new Callback<List<WRDataGeneral>>() {
//            @Override
//            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
//                if (response.isSuccessful()) {
//                    arrayListByGoodsID = (ArrayList<WRDataGeneral>) response.body();
//                    if (arrayListByGoodsID.size() > 0) {
//                        //isGoodsIDExist = true;
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return arrayListByGoodsID;
//    }

//    private ArrayList<WRDataDetail> fetchWRDataDetailByIdCode(String wrdNumber, String goodsId, String idCode) {
//        Call<List<WRDataDetail>> callback = iWarehouseApi.getWRDataDetailByIdCode(wrdNumber, goodsId, idCode);
//        callback.enqueue(new Callback<List<WRDataDetail>>() {
//            @Override
//            public void onResponse(Call<List<WRDataDetail>> call, Response<List<WRDataDetail>> response) {
//                if (response.isSuccessful()) {
//                    arrayListByIDCode = new ArrayList<>();
//                    arrayListByIDCode = (ArrayList<WRDataDetail>) response.body();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WRDataDetail>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return arrayListByIDCode;
//    }

    private void putWRDataHeader(String wRDNumber, WRDataHeader wrDataHeader) {
        Call<WRDataHeader> callback = iWarehouseApi.updateWRDataHeader(wRDNumber, wrDataHeader);
        callback.enqueue(new Callback<WRDataHeader>() {
            @Override
            public void onResponse(Call<WRDataHeader> call, Response<WRDataHeader> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataHeaderData(wRDNumber);
                }
            }

            @Override
            public void onFailure(Call<WRDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdataheader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWRDataGeneral(String wRDNumber, String goodsID, int ordinal, WRDataGeneral wrDataGeneral) {
        Call<WRDataGeneral> callback = iWarehouseApi.updateWRDataGeneral(wRDNumber, goodsID, ordinal, wrDataGeneral);
        callback.enqueue(new Callback<WRDataGeneral>() {
            @Override
            public void onResponse(Call<WRDataGeneral> call, Response<WRDataGeneral> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataGeneralData(wRDNumber);
                    getTotalQuantity(wrDataGenerals);
                    getTotalGoods(wrDataGenerals);
                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                    edtTotalGoods.setText(String.format("%.0f", totalGoods));
                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WRDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void putWRDataDetail(String wRDNumber, String goodsID, int ordinal, WRDataDetail wrDataDetail) {
//        Call<WRDataDetail> callback = iWarehouseApi.updateWRDataDetail(wRDNumber, goodsID, ordinal, wrDataDetail);
//        callback.enqueue(new Callback<WRDataDetail>() {
//            @Override
//            public void onResponse(Call<WRDataDetail> call, Response<WRDataDetail> response) {
//                if (response.isSuccessful()) {
//                    fetchWRDataDetailData(wRDNumber);
//                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
//                } else {
//                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<WRDataDetail> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void getTotalQuantity(ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.size() > 0 && wrDataGenerals.get(i).getQuantity() > 0) {
                totalQuantity += wrDataGenerals.get(i).getQuantity();
            }
        }
    }

    private void getTotalGoods(ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0) {
                totalGoods += 1;
            }
        }
    }

    private Boolean checkGoodsIDDuplicate(ArrayList<WRDataGeneral> wrDataGenerals, String barcode) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getBarcode().equalsIgnoreCase(barcode)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }

    private Boolean checkExistGoodsID(String goodsID, ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (goodsID.equalsIgnoreCase(wrDataGenerals.get(i).getBarcode())) {
                isGoodsIDExist = true;
                break;
            }
        }
        return isGoodsIDExist;
    }

//    private Boolean checkExistGoodsIDAndIDCode(String goodsID, String idCode, ArrayList<WRDataDetail> arrayList) {
//        for (int i = 0; i < arrayList.size(); i++) {
//            if (goodsID.equalsIgnoreCase(arrayList.get(i).getGoodsID()) && idCode.equalsIgnoreCase(arrayList.get(i).getIdCode())) {
//                isGoodsIDAndIDCodeExist = true;
//                break;
//            }
//        }
//        return isGoodsIDAndIDCodeExist;
//    }

//    private Boolean checkGoodsIDAndIDCodeScaned(String goodsID, String idCode, ArrayList<WRDataDetail> wrDataDetails) {
//        for (int i = 0; i < wrDataDetails.size(); i++) {
//            if (goodsID.equalsIgnoreCase(wrDataDetails.get(i).getGoodsID()) && idCode.equalsIgnoreCase(wrDataDetails.get(i).getIdCode())
//                    && wrDataDetails.get(i).getQuantity().doubleValue() > 0) {
//                isGoodsIDAndIDCodeScanned = true;
//                break;
//            }
//        }
//        return isGoodsIDAndIDCodeScanned;
//    }

    private ArrayList<WRDataGeneral> getNotYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() == 0) {
                notYetList.add(wrDataGenerals.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WRDataGeneral> getEnoughtYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughYettList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity() < wrDataGenerals.get(i).getQuantityOrg()) {
                enoughYettList.add(wrDataGenerals.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WRDataGeneral> getEnoughtList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity() == wrDataGenerals.get(i).getQuantityOrg()) {
                enoughtList.add(wrDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WRDataGeneral> getOvertList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> overList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > wrDataGenerals.get(i).getQuantityOrg()) {
                overList.add(wrDataGenerals.get(i));
            }
        }
        return overList;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void setAdapter(ArrayList<WRDataGeneral> wrDataGenerals) {
        adapter = new WRDataGeneralBarcodeAdapter(getApplicationContext(), wrDataGenerals, new WRDataGeneralBarcodeAdapter.ItemClickListener() {
            @Override
            public void onClick(WRDataGeneral wrDataGeneral) {
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_wrdatadetail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<WRDataGeneral> allList = wrDataGenerals;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_yet) {
            ArrayList<WRDataGeneral> notYetList = getNotYetList(wrDataGenerals);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_enought_yet) {
            ArrayList<WRDataGeneral> enoughtYetLish = getEnoughtYetList(wrDataGenerals);
            setAdapter(enoughtYetLish);
            return true;
        }
        if (id == R.id.action_enought) {
            ArrayList<WRDataGeneral> enoughtList = getEnoughtList(wrDataGenerals);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_over) {
            ArrayList<WRDataGeneral> overList = getOvertList(wrDataGenerals);
            setAdapter(overList);
            return true;
        }
        if (id == R.id.action_save) {
            process();
            return true;
        }
        if (id == R.id.action_approve) {
            if (wrDataHeader != null) {
                WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                wrDataHeaderUpdate.setHandlingStatusID("2");
                wrDataHeaderUpdate.setHandlingStatusName("Duyệt mức 2");
                putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                alertSuccess(getApplicationContext(), "Duyệt mức 2 nhập kho thành công");
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (edtWRDNumber.hasFocus()) {
                        edtWRDNumber.setText(result);
                    }
                    String goodsID = "";
                    String quantity = "";
//                    String idCode = "";
                    int index1 = result.lastIndexOf("-");
                    if (index1 > 0) {
                        goodsID = result.substring(0, index1);
                        quantity = result.substring(index1 + 1, result.length());
//                        idCode = result.substring(index2 + 1, result.length());
                    }
                    edtGoodsID.setText(goodsID);
//                    edtIDCode.setText(idCode);
                    edtQuantity.setText(quantity);

                    process();
                }
            }
        });
    }

    private void process() {
        if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
            alertError(getApplicationContext(), "Nhập mã hàng");
            edtGoodsID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
            alertError(getApplicationContext(), "Nhập số lượng");
            edtQuantity.requestFocus();
            return;
        }
//        if (!edtQuantity.getText().toString().matches(numberRegex)) {
//            alertError(getApplicationContext(), "Không phải là số");
//            return;
//        }

        isGoodsIDExist = false;
        checkExistGoodsID(edtGoodsID.getText().toString(), wrDataGenerals);
        if (isGoodsIDExist == true) {
            isGoodsIDExist = false;
        } else {
            alertError(getApplicationContext(), "Mã hàng không tồn tại");
            return;
        }

        totalQuantity = 0.0;
        totalGoods = 0.0;
        getTotalGoods(wrDataGenerals);
        getTotalQuantity(wrDataGenerals);
        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralByBarcode(edtWRDNumber.getText().toString(), edtGoodsID.getText().toString());
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    arrayListByGoodsID = (ArrayList<WRDataGeneral>) response.body();
                    if (arrayListByGoodsID.size() > 0) {
                        WRDataGeneral wrDataGeneralUpdate = arrayListByGoodsID.get(0);
                        checkGoodsIDDuplicate(arrayListByGoodsID, edtGoodsID.getText().toString());
                        if (isGoodsIdDuplicate == false) {
                            wrDataGeneralUpdate.setQuantity(Integer.parseInt(edtQuantity.getText().toString()));
                        } else {
                            wrDataGeneralUpdate.setQuantity(wrDataGeneralUpdate.getQuantity() + Integer.parseInt(edtQuantity.getText().toString()));
                            isGoodsIdDuplicate = false;
                        }
                        wrDataGeneralUpdate.setWrdNumber(edtWRDNumber.getText().toString());
//                        wrDataGeneralUpdate.setGoodsID(edtGoodsID.getText().toString());
                        wrDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());
//                        wrDataGeneralUpdate.setTotalGoods(totalGoods + 1);
//                        wrDataGeneralUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        putWRDataGeneral(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getBarcode(), wrDataGeneralUpdate.getOrdinal(), wrDataGeneralUpdate);

                        WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                        wrDataHeaderUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }
}