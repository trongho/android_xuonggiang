package com.example.warehousemobileclient.ui.warehouse;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.databinding.FragmentWarehouseBinding;
import com.example.warehousemobileclient.ui.got_goods_data.GotGoodsActivity;
import com.example.warehousemobileclient.ui.issue_data.IssueDataActivity;
import com.example.warehousemobileclient.ui.receipt_data.ReceiptDataActivityRFID;
import com.example.warehousemobileclient.ui.sort_goods_data.SortGoodsDataActivity;
import com.example.warehousemobileclient.ui.tally.TallyDataActivity;

public class WarehouseFragment extends Fragment {

    private WarehouseViewModel warehouseViewModel;
    private FragmentWarehouseBinding binding;
    public static Boolean isLogin=false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        warehouseViewModel =
                new ViewModelProvider(this).get(WarehouseViewModel.class);

        binding = FragmentWarehouseBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final LinearLayout llWarehouseReceipt = binding.llWarehouseReceipt;
        final LinearLayout llIssueOrder = binding.llIssueOrder;
        final LinearLayout llWarehouseReceiptData = binding.llWarehouseReceiptData;
        final LinearLayout llIssueData = binding.llIssueData;
        final LinearLayout llTally = binding.llTally;
        final LinearLayout llGotGoodsData = binding.llGotGoodsData;
        final LinearLayout llSortGoodsData= binding.llSortGoodsData;
        final LinearLayout llLogout = binding.llLogout;

        llIssueOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Navigation.findNavController(getActivity(),R.id.nav_host_fragment_activity_main).navigate(R.id.issueOrderdest, null,getNavOptions());
            }
        });

        llWarehouseReceipt.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.warehouse_receipt_action, null));
        llIssueOrder.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.issue_order_action, null));
        llWarehouseReceiptData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ReceiptDataActivityRFID.class));
            }
        });
        llIssueData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), IssueDataActivity.class));
            }
        });
        llGotGoodsData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), GotGoodsActivity.class));
            }
        });
        llSortGoodsData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), SortGoodsDataActivity.class));
            }
        });
        llTally.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), TallyDataActivity.class));
            }
        });

        if(isLogin==true){
            binding.iv1.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv2.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv3.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv4.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv5.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv6.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv7.setImageResource(R.drawable.ic_baseline_receipt_24);
            llWarehouseReceiptData.setEnabled(true);
            llWarehouseReceipt.setEnabled(true);
            llIssueOrder.setEnabled(true);
            llIssueData.setEnabled(true);
            llGotGoodsData.setEnabled(true);
            llSortGoodsData.setEnabled(true);
            llTally.setEnabled(true);
        }else {
            llWarehouseReceiptData.setEnabled(false);
            llWarehouseReceipt.setEnabled(false);
            llIssueOrder.setEnabled(false);
            llIssueData.setEnabled(false);
            llGotGoodsData.setEnabled(false);
            llSortGoodsData.setEnabled(false);
            llTally.setEnabled(false);
        }
        
        return root;
    }



    protected NavOptions getNavOptions() {
        NavOptions navOptions = new NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in_right)
                .setExitAnim(R.anim.slide_out_left)
                .setPopEnterAnim(R.anim.slide_in_left)
                .setPopExitAnim(R.anim.slide_out_right)
                .build();
        return navOptions;
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}