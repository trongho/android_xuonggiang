package com.example.warehousemobileclient.ui.tally;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.TallyDataAdapter;
import com.example.warehousemobileclient.helper.Common;
import com.example.warehousemobileclient.model.TSDetail;
import com.example.warehousemobileclient.model.TSHeader;
import com.example.warehousemobileclient.model.TallyData;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TallyDataActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private RecyclerView recyclerView = null;
    private EditText edtTSNumber = null;
    private EditText edtGoodsID = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalRow = null;
    private Button btnClearTSNumber = null;
    private Button btnClearGoodsID = null;
    private Button btnClearQuantity = null;
    private TallyDataAdapter adapter;
    ArrayList<TallyData> tallyDatas;
    ArrayList<TSHeader> tsHeaders;
    ArrayList<TSDetail> tsDetails;
    ArrayList<TSDetail> tsDetailByMultiIDs;
    private Double totalQuantity = 0.0;
    private Double totalRow = 0.0;
    private int No = 0;
    private int Ordinal = 0;
    Boolean isExistTSHeader=false;

    private String autoTSNumber;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Calendar c = Calendar.getInstance();

    IWarehouseApi iWarehouseApi = RetrofitService.getService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tally_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Kiểm kê");


        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();
        tallyDatas = new ArrayList<TallyData>();
        tsHeaders = new ArrayList<>();
        tsDetails = new ArrayList<TSDetail>();
        tsDetailByMultiIDs = new ArrayList<TSDetail>();

        edtTSNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu kiểm kê");
                    edtTSNumber.requestFocus();
                    return;
                }
                if (editable.length() == 11) {
                    FetchTallyData(edtTSNumber.getText().toString());
                    if (tallyDatas.size() > 0) {
                        alertSuccess(getApplicationContext(), "Lấy dữ liệu kiểm kê thành công");
                        edtGoodsID.requestFocus();
                    } else {
                        alertError(getApplicationContext(), "Không tìm thấy chi tiết dữ liệu kiểm kê");
                    }
                }
            }
        });

        edtGoodsID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Scan mã hàng");
                    edtGoodsID.requestFocus();
                    return;
                }
                edtQuantity.requestFocus();
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số lượng");
                    edtQuantity.requestFocus();
                    return;
                }
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtTSNumber = findViewById(R.id.edtTSNumber);
        edtGoodsID = findViewById(R.id.edtGoodsID);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearTSNumber = findViewById(R.id.btn_clear_tsnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalRow = findViewById(R.id.edtTotalRow);
    }

    private Double getTotalQuantity(ArrayList<TallyData> tallyDatas) {
        for (int i = 0; i < tallyDatas.size(); i++) {
            if (tallyDatas.size() > 0 && tallyDatas.get(i).getQuantity() > 0) {
                totalQuantity += tallyDatas.get(i).getQuantity();
            }
        }
        return totalQuantity;
    }

    private Double getTotalRow(ArrayList<TallyData> tallyDatas) {
        for (int i = 0; i < tallyDatas.size(); i++) {
            totalRow += 1;
        }
        return totalRow;
    }

    private void setAdapter(ArrayList<TallyData> tallyDatas) {
        adapter = new TallyDataAdapter(getApplicationContext(), tallyDatas, new TallyDataAdapter.ItemClickListener() {
            @Override
            public void onClick(TallyData tallyData) {

            }
        }, new TallyDataAdapter.ItemActionListener() {
            @Override
            public void onClick(TallyData tallyData, int position) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    popupMenuClick(position, tallyData);
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void popupMenuClick(int position, TallyData tallyData) {
        PopupMenu popup = new PopupMenu(getApplicationContext(), recyclerView.getChildAt(position));
        popup.setGravity(Gravity.END);
        popup.inflate(R.menu.action_edit_delete_menu);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menuItem_edit:
                        dialogEditTallyData(tallyData);
                        break;
                    case R.id.menuItem_delete:
                        dialogDeleteTallyDataDetail(tallyData);
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private void dialogEditTallyData(TallyData tallyData) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(TallyDataActivity.this, R.style.myDialog));
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_edit_tally_data, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                FetchTallyData(edtTSNumber.getText().toString());
            }
        });
        final EditText edtGoodsID = alertDialog.findViewById(R.id.edtGoodsID);
        final EditText edtQuantity = alertDialog.findViewById(R.id.edtQuantity);
        final EditText edtNo = alertDialog.findViewById(R.id.edtNo);
        Button btnSave = alertDialog.findViewById(R.id.btnSave);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        edtGoodsID.setText(tallyData.getGoodsID());
        edtQuantity.setText(tallyData.getQuantity() + "");
        edtNo.setText(tallyData.getNo() + "");
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TallyData tallyDataUpdate = tallyData;
                tallyData.setGoodsID(edtGoodsID.getText().toString());
                tallyData.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                PutTallyData(edtTSNumber.getText().toString(), edtGoodsID.getText().toString(),
                        Integer.parseInt(edtNo.getText().toString()), tallyDataUpdate);
                hideSoftKeyBoard();
                alertDialog.cancel();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    private void dialogDeleteTallyDataDetail(final TallyData tallyData) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(TallyDataActivity.this, R.style.myDialog));
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                FetchTallyData(edtTSNumber.getText().toString());
            }
        });

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteTallyDataDetail(tallyData.getTsNumber(), tallyData.getGoodsID(), tallyData.getNo());
                alertDialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    private void dialogDeleteTallyData(String tsNumber) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(TallyDataActivity.this, R.style.myDialog));
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                FetchTallyData(edtTSNumber.getText().toString());
            }
        });

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteTallyData(tsNumber);
                alertDialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }


    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private String getLastID() {
        Call<String> callback = iWarehouseApi.getLastTSNumber();
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    autoTSNumber = response.body().toString();
                    Calendar c = Calendar.getInstance();
                    String curentYear = originalFormat.format(c.getTime()).substring(2, 4);
                    if (autoTSNumber.equalsIgnoreCase("")) {
                        edtTSNumber.setText(curentYear + "000" + "-" + String.format("%05d", 1));
                    } else {
                        String[] arrListStr = autoTSNumber.split("-");
                        String tsNumberPartOne = arrListStr[0];
                        String tsNumberPartTwo = arrListStr[1];
                        int tsNumberPartTwoInt = Integer.parseInt(tsNumberPartTwo) + 1;
                        edtTSNumber.setText(curentYear + "000" + "-" + String.format("%05d", tsNumberPartTwoInt));
                    }
                } else {
                    alertError(getApplicationContext(), "Không tìm thấy tsNumber");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch tsNumber fail", Toast.LENGTH_SHORT).show();
            }
        });
        return autoTSNumber;
    }

    private void PostTallyData(TallyData tallyData) {
        Call<TallyData> callback = iWarehouseApi.creatTallyData(tallyData);
        callback.enqueue(new Callback<TallyData>() {
            @Override
            public void onResponse(Call<TallyData> call, Response<TallyData> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Creat success tallydata");
                    FetchTallyData(edtTSNumber.getText().toString());
                } else {
                    Toast.makeText(getApplicationContext(), "Creat fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TallyData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PutTallyData(String tsNumber, String goodsID, int No, TallyData tallyData) {
        Call<TallyData> callback = iWarehouseApi.updateTallyData(tsNumber, goodsID, No, tallyData);
        callback.enqueue(new Callback<TallyData>() {
            @Override
            public void onResponse(Call<TallyData> call, Response<TallyData> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Upadte success tallydata");
                    FetchTallyData(edtTSNumber.getText().toString());
                } else {
                    Toast.makeText(getApplicationContext(), "Update fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TallyData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void DeleteTallyDataDetail(String tsNumber, String goodsID, int No) {
        Call<Boolean> callback = iWarehouseApi.deleteTallyDataDetail(tsNumber, goodsID, No);
        callback.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Delete success tallydata");
                } else {
                    Toast.makeText(getApplicationContext(), "Delete fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void DeleteTallyData(String tsNumber) {
        Call<Boolean> callback = iWarehouseApi.deleteTallyData(tsNumber);
        callback.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Delete success tallydata");
                } else {
                    Toast.makeText(getApplicationContext(), "Delete fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<TallyData> FetchTallyData(String tsNumber) {
        Call<List<TallyData>> callback = iWarehouseApi.getTallyDataById(tsNumber);
        callback.enqueue(new Callback<List<TallyData>>() {
            @Override
            public void onResponse(Call<List<TallyData>> call, Response<List<TallyData>> response) {
                if (response.isSuccessful()) {
                    tallyDatas = (ArrayList<TallyData>) response.body();
                    setAdapter(tallyDatas);
                    totalQuantity = 0.0;
                    totalRow = 0.0;
                    edtTotalQuantity.setText(getTotalQuantity(tallyDatas) + "");
                    edtTotalRow.setText(getTotalRow(tallyDatas) + "");
                } else {
                    Toast.makeText(getApplicationContext(), "fetch dail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<TallyData>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
            }
        });
        return tallyDatas;
    }

    private ArrayList<TSHeader> FetchTSHeader(String tsNumber) {
        Call<List<TSHeader>> callback = iWarehouseApi.getTSHeaderById(tsNumber);
        callback.enqueue(new Callback<List<TSHeader>>() {
            @Override
            public void onResponse(Call<List<TSHeader>> call, Response<List<TSHeader>> response) {
                if (response.isSuccessful()) {
                    tsHeaders = (ArrayList<TSHeader>) response.body();
                    Toast.makeText(TallyDataActivity.this,tsHeaders.size()+"",Toast.LENGTH_SHORT).show();
                    isExistTSHeader=true;
                } else {
                    Toast.makeText(getApplicationContext(), "fetch fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<TSHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
            }
        });
        return tsHeaders;
    }

    private ArrayList<TSDetail> FetchTSDetailByMultiID(String tsNumber, String goodsId, int ordinal) {
        Call<List<TSDetail>> callback = iWarehouseApi.getTSDetailByMultiId(tsNumber, goodsId, ordinal);
        callback.enqueue(new Callback<List<TSDetail>>() {
            @Override
            public void onResponse(Call<List<TSDetail>> call, Response<List<TSDetail>> response) {
                if (response.isSuccessful()) {
                    tsDetails = (ArrayList<TSDetail>) response.body();
                } else {
                    Toast.makeText(getApplicationContext(), "fetch dail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<TSDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
            }
        });
        return tsDetailByMultiIDs;
    }

    private ArrayList<TSDetail> FetchTSDetail(String tsNumber) {
        Call<List<TSDetail>> callback = iWarehouseApi.getTSDetailById(tsNumber);
        callback.enqueue(new Callback<List<TSDetail>>() {
            @Override
            public void onResponse(Call<List<TSDetail>> call, Response<List<TSDetail>> response) {
                if (response.isSuccessful()) {
                    tsDetails = (ArrayList<TSDetail>) response.body();
                } else {
                    Toast.makeText(getApplicationContext(), "fetch dail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<TSDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
            }
        });
        return tsDetails;
    }

    private void PostTSHeader(TSHeader tsHeader) {
        Call<TSHeader> callback = iWarehouseApi.creatTSHeader(tsHeader);
        callback.enqueue(new Callback<TSHeader>() {
            @Override
            public void onResponse(Call<TSHeader> call, Response<TSHeader> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Creat success");
                } else {
                    Toast.makeText(getApplicationContext(), "Creat fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TSHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PutTSHeader(String tsNumber, TSHeader tsHeader) {
        Call<TSHeader> callback = iWarehouseApi.updateTSHeader(tsNumber, tsHeader);
        callback.enqueue(new Callback<TSHeader>() {
            @Override
            public void onResponse(Call<TSHeader> call, Response<TSHeader> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Upadte success tallydata");
                    FetchTallyData(edtTSNumber.getText().toString());
                } else {
                    Toast.makeText(getApplicationContext(), "Update fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TSHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PostTSDetail(TSDetail tsDetail) {
        Call<TSDetail> callback = iWarehouseApi.creatTSDetail(tsDetail);
        callback.enqueue(new Callback<TSDetail>() {
            @Override
            public void onResponse(Call<TSDetail> call, Response<TSDetail> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Creat success");
                } else {
                    Toast.makeText(getApplicationContext(), "Creat fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TSDetail> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PutTSDetail(String tsNumber, String goodsID, int ordinal, TSDetail tsDetail) {
        Call<TSDetail> callback = iWarehouseApi.updateTSDetail(tsNumber, goodsID, ordinal, tsDetail);
        callback.enqueue(new Callback<TSDetail>() {
            @Override
            public void onResponse(Call<TSDetail> call, Response<TSDetail> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Upadte success tallydata");
                    FetchTallyData(edtTSNumber.getText().toString());
                } else {
                    Toast.makeText(getApplicationContext(), "Update fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TSDetail> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_tally_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_addnew) {
            getLastID();
            edtGoodsID.requestFocus();
            return true;
        }
        if (id == R.id.action_save) {
            if (TextUtils.isEmpty(edtTSNumber.getText().toString())) {
                alertError(getApplicationContext(), "Nhập số phiếu kiểm kê");
                edtTSNumber.requestFocus();
            } else if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
                alertError(getApplicationContext(), "Scan mã hàng");
                edtGoodsID.requestFocus();
            } else if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
                alertError(getApplicationContext(), "Nhập số lượng");
                edtQuantity.requestFocus();
            } else {
                TallyData tallyData = new TallyData();
                tallyData.setTsNumber(edtTSNumber.getText().toString());
                tallyData.setTsDate(originalFormat.format(c.getTime()));
                tallyData.setGoodsID(edtGoodsID.getText().toString());
                tallyData.setGoodsName("");
                tallyData.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                tallyData.setCreatorID(Common.USER_ID);
                tallyData.setCreatedDateTime(originalFormat.format(c.getTime()));
                tallyData.setStatus("0");
                if (tallyDatas.size() > 0) {
                    tallyData.setTotalQuantity(getTotalQuantity(tallyDatas) + Double.parseDouble(edtQuantity.getText().toString()));
                    tallyData.setTotalRow(getTotalRow(tallyDatas) + 1);
                } else {
                    tallyData.setTotalQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                    tallyData.setTotalRow(1.0);
                }
                tallyData.setNo(No + 1);
                PostTallyData(tallyData);
                No = No + 1;
                hideSoftKeyBoard();
            }
            return true;
        }
        if (id == R.id.action_delete) {
            dialogDeleteTallyData(edtTSNumber.getText().toString());
            return true;
        }
        if (id == R.id.action_approve) {
            FetchTSHeader(edtTSNumber.getText().toString());
            Toast.makeText(TallyDataActivity.this,tsHeaders.size()+"",Toast.LENGTH_SHORT).show();
            if (tsHeaders.size()>0) {
                Toast.makeText(getApplicationContext(), "aaa", Toast.LENGTH_SHORT).show();
                TSHeader tsHeaderUpdate = tsHeaders.get(0);
                tsHeaderUpdate.setStatus("1");
                try {
                    tsHeaderUpdate.setTsDate(originalFormat.parse(originalFormat.format(c.getTime())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                tsHeaderUpdate.setUpdatedUserID(Common.USER_ID);
                try {
                    tsHeaderUpdate.setUpdatedDate(originalFormat.parse(originalFormat.format(c.getTime())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                tsHeaderUpdate.setTotalQuantity(Double.valueOf(edtTotalQuantity.getText().toString()));
                PutTSHeader(edtTSNumber.getText().toString(), tsHeaderUpdate);

                for (int i = 0; i < tallyDatas.size(); i++) {
                    FetchTSDetailByMultiID(tallyDatas.get(i).getTsNumber(), tallyDatas.get(i).getGoodsID(), tallyDatas.get(i).getNo());


                    if (tsDetailByMultiIDs.size() > 0) {
                        Toast.makeText(getApplicationContext(), "ccc", Toast.LENGTH_SHORT).show();
                        TSDetail tsDetailUpdate = tsDetailByMultiIDs.get(i);
                        tsDetailUpdate.setTsNumber(edtTSNumber.getText().toString());
                        tsDetailUpdate.setGoodsID(tallyDatas.get(i).getGoodsID());
                        tsDetailUpdate.setQuantity(tallyDatas.get(i).getQuantity());
                        tsDetailUpdate.setOrdinal(tallyDatas.get(i).getNo());
                        tsDetailUpdate.setStatus("1");
                        PutTSDetail(tsDetailUpdate.getTsNumber(), tsDetailUpdate.getGoodsID(), tsDetailUpdate.getOrdinal(), tsDetailUpdate);
                    } else {
                        Toast.makeText(getApplicationContext(), "ddd", Toast.LENGTH_SHORT).show();
                        TSDetail tsDetailUpdate = new TSDetail();
                        tsDetailUpdate.setTsNumber(edtTSNumber.getText().toString());
                        tsDetailUpdate.setGoodsID(tallyDatas.get(i).getGoodsID());
                        tsDetailUpdate.setQuantity(tallyDatas.get(i).getQuantity());
                        tsDetailUpdate.setOrdinal(tallyDatas.get(i).getNo());
                        tsDetailUpdate.setStatus("0");
                        PostTSDetail(tsDetailUpdate);
                    }
                }
            } else {
                Toast.makeText(getApplicationContext(), "bbb", Toast.LENGTH_SHORT).show();
                TSHeader tsHeader = new TSHeader();
                tsHeader.setTsNumber(edtTSNumber.getText().toString());

                try {
                    tsHeader.setTsDate(originalFormat.parse(originalFormat.format(c.getTime())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                tsHeader.setHandlingStatusID("1");
                tsHeader.setHandlingStatusName("Duyệt mức 1");
                tsHeader.setTotalQuantity(Double.valueOf(edtTotalQuantity.getText().toString()));
                tsHeader.setStatus("0");
                tsHeader.setCreatedUserID(Common.USER_ID);

                try {
                    tsHeader.setCreatedDate(originalFormat.parse(originalFormat.format(c.getTime())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                PostTSHeader(tsHeader);

                for (int i = 0; i < tallyDatas.size(); i++) {
                    TSDetail tsDetail = new TSDetail();
                    tsDetail.setTsNumber(edtTSNumber.getText().toString());
                    tsDetail.setGoodsID(tallyDatas.get(i).getGoodsID());
                    tsDetail.setQuantity(tallyDatas.get(i).getQuantity());
                    tsDetail.setOrdinal(tallyDatas.get(i).getNo());
                    tsDetail.setStatus("0");
                    PostTSDetail(tsDetail);
                }
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.action_edit_delete_menu, menu);
//    }
//
//    @Override
//    public boolean onContextItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menuItem_edit:
//                Toast.makeText(this, "Bookmark", Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.menuItem_delete:
//                Toast.makeText(this, "Upload", Toast.LENGTH_SHORT).show();
//            default:
//                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
//                break;
//        }
//        return true;
//    }

    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (edtGoodsID.hasFocus()) {
                        edtGoodsID.setText(result);
                    }
                }
            }
        });
    }
}